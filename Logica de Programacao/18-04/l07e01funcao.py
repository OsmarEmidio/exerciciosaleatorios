'''
def função(arg1, arg2, ... , argn):
    script

    return val1, val2, ... , valn

def --> declara que estamos construindo uma funçao
função --> bome da função
script --> Código da função
return --> Indica o fim da função e o que estará sendo retornado

val1, val2, ... , valn --> Valores retornados

ex:
print() é uma função, ou seja, a ideia de criar uma função
é a sua reutilização em varios momentos do programa


1.Escreva uma função de area
'''
# declaração da função
def area(base, altura):
    area = base*altura
    return area
base:float(input("Valor da Base: "))
altura:float(input("Valor da Altura: "))
resp=area(base,altura)
print("A área é igual a ",resp)
#chamando a função dentro do print

'''    Alterações:
a. Criar uma variavel para receber o valor retornado da função
a1. Experimente colocar esta linha antes da declaração da função. O que acontece?
a2. coloque no lugar correto.
b. Fazer com que o usuario entre com os valores de entrada (base, altura).
c. Peça para o usuario digitar a base e a altura 3 vezes e depois imprima a soma total


'''
