print("Equação de Segundo Grau")
a=float(input("Valor de a: "))
b=float(input("Valor de b: "))
c=float(input("Valor de c: "))
import math
d=pow(b,2)-(4*a*c)
if(a==0):
    print("O a não pode ser 0, pois não é possivel dividir um numero por 0")
elif(d<0):
    print("Não existem raizes reais nessa equação")
elif(d==0):
    x1=((-b)+math.sqrt(d))/(2*a)
    print("As duas raizes valem {:.0f}".format(x1))
else:
    x2=((-b)+(math.sqrt(d)))/(2*a)
    x3=((-b)-(math.sqrt(d)))/(2*a)
    print("A primeira raiz vale {:.0f} e a segunda raiz vale {:.0f}".format(x2,x3))
