'''
def função(arg1, arg2, ... , argn):
    script

    return val1, val2, ... , valn

def --> declara que estamos construindo uma funçao
função --> bome da função
script --> Código da função
return --> Indica o fim da função e o que estará sendo retornado

val1, val2, ... , valn --> Valores retornados

3.Escreva uma função que recebe dois números e devolve a soma e
a multiplicação entre os dois
'''
# declaração da função soma
def opMat(num1, num2):
    somaT=num1+num2
    mult=num1*num2
    return somaT, mult #retorna dois valores 

#chamando a função dentro do print


print(opMat(2,3)) # a chamada da função deverá sempre ser utilizada
                 #após a criação da função


'''    Alterações:

a. Fazer com que o usuario entre com os valores de entrada (num1, num2).
b = criar duas variaveis para receber os valores retornados pela função
opMat(num1, num2)
dica:

a, b = x, y # a receberá o valor de x; b receberá o valor de y

'''
