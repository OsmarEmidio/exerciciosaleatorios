import sys

print("-----------------------------------------------------------------------------")
print("UNICEUB - Ciência da Computação")
print("Estrutura de Dados - 2º Semestre")
print("Aluno: Osmar Emídio - 21805098")
print("-----------------------------------------------------------------------------")
try:
	arquivo=open("Agenda.txt","r")
except FileNotFoundError:
    arquivo = open("Agenda.txt", 'w+')
arquivo.close

class No():
	def __init__(self,dado):
			self.dado = dado
			self.proximo = None

class ListaEncadeada:
	def __init__(self):
		self.head = None
		self.size = 0

	def append(self,elemento):
		if (self.head):
			ponteiro = self.head
			while(ponteiro.proximo):
				ponteiro = ponteiro.proximo
			ponteiro.proximo = No(elemento)
		else:
			self.head = No(elemento)
		self.size += 1

	def len(self):
		return self.size

	def set(self, index, elemento):
		ponteiro = self.head
		for i in range(index):
			if ponteiro:
				ponteiro=ponteiro.proximo
			else:
				raise IndexError("list index out of range")
		if ponteiro:
			ponteiro.dado = elemento
		else:
			raise IndexError("list index out of range")

	def get(self, index):
		ponteiro = self.head
		for i in range(index):
			if ponteiro:
				ponteiro=ponteiro.proximo
			else:
				raise IndexError("list index out of range")
		if ponteiro:
			return ponteiro.dado
		else:
			raise IndexError("list index out of range")

	def index(self,elemento):
		ponteiro = self.head
		i = 0
		while ponteiro:
			if (ponteiro.dado == elemento):
				return i
			else:
				ponteiro = ponteiro.proximo
				i += 1
		raise ValueError("O Elemento não está na lista")

	def remove(self,elemento):
		if(self.head == None):
			raise ValueError()
		elif(self.head.dado == elemento):
			self.head=self.head.proximo
			self.size=self.size-1
			return True
		else:
			ancestor=self.head
			ponteiro=self.head.proximo
			while(ponteiro):
				if(ponteiro.dado == elemento):
					ancestor.proximo=ponteiro.proximo
					ponteiro.proximo=None
				ancestor=ponteiro
				ponteiro=ponteiro.proximo
			self.size=self.size-1
			return True
		raise ValueError()

t=sum(1 for line in open("Agenda.txt"))
l=1
lista = ListaEncadeada()
arquivo=open("Agenda.txt","r")
for line in arquivo:
	if(line == ""):
		pass
	else:
		elemento=line[:-1]
		lista.append(elemento)
		l+=1
arquivo.close()

class Menu():
	def __init__(self):
		print("---------------------------------Agenda--------------------------------------")
		print("[0] - Sair")
		print("[1] - Vizualizar Contatos")
		print("[2] - Adicionar Contatos")
		print("[3] - Remover Contatos")
		print("")
		o=int(input("Opção: "))
		print("-----------------------------------------------------------------------------")
		print("")
		if(o == 0):
			sys.exit()
		elif(o == 1):
			agenda.vizualizar()
		elif(o == 2):
			agenda.addcontato()
		elif(o == 3):
			agenda.removercontato()
		else:
			print("Digite um comando valido (um nº de 0 a 3)")
		print("")
		print("-----------------------------------------------------------------------------")
		Menu()

class Agenda():
	def vizualizar(self):
		for i in range(lista.size):
			print("{}. ".format(i+1), end="")
			print(lista.get(i))

	def addcontato(self):
		print("Obs: Digite conforme o seguinte padrão: Nome - Numero")
		contato=input("Novo Contato: ")
		lista.append(contato)
		arquivo=open("Agenda.txt","a")
		arquivo.write(contato + "\n")
		arquivo.close()

	def removercontato(self):
		print("Obs: A linha aparece ao lado do contato ao vizualizar os contatos!")
		l=int(input("Linha do Contato a ser Removido: "))
		i=1
		na=[]
		elemento=0
		arquivo=open("Agenda.txt","r")
		for line in arquivo:
			if(i == l):
				elemento = line[:-1]
				lista.remove(elemento)
				i+=1
			else:
				na.append(line)
				i+=1
		arquivo.close()
		arquivo=open("Agenda.txt","w")
		for i in range(0,len(na)):
			arquivo.write(na[i])
		arquivo.close()
		if(elemento == 0):
			print("Linha não encontrada! Tente novamente!")
			print("")
			agenda.removercontato()
			


agenda = Agenda()
Menu()
