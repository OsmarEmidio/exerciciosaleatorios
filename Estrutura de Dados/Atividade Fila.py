import sys
import os
from random import randrange

print("-----------------------------------------------------------------------------")
print("UNICEUB - Ciência da Computação")
print("Estrutura de Dados - 2º Semestre")
print("Aluno: Osmar Emídio - 21805098")
print("-----------------------------------------------------------------------------")

class Fila():
    def __init__(self):
        self.fila = []

    def Inserir_Int(self):
        n=int(input("Quantos elementos você deseja inserir à fila: "))
        for i in range(1,n+1):
            e=int(input("{}º Elemento a ser adicionado: ".format(i)))
            self.fila.append(e)

    def Inserir_Input(self):
        n=int(input("Quantos elementos você deseja inserir à fila: "))
        for i in range(1,n+1):
            e=input("{}° Elemento a ser adicionado: ".format(i))
            self.fila.append(e)

    def Remover(self):
        self.p = self.fila.pop(0)

    def Print(self):
        print("Fila: ",self.fila)

class Menu():
    def Painel():
        print("============ PARA VER UM EXERCÍCIO DIGITE SEU RESPECTIVO NÚMERO =============")
        print("")
        print("Digite [0] para sair do programa")
        print("Exercício 1 - Inserir n elementos em uma fila e em seguida retirar os elementos e inserir novamente somente os pares")
        print("Exercício 2 - Inverter uma fila usando pilha")
        print("Exercício 3 - Eliminar todos os elementos ímpares de uma fila do tipo inteiro natural")
        print("Exercício 4 - Excluir todos os inteiros negativos de uma fila")
        print("Exercício 5 - Inverter o conteúdo de uma fila")
        print("Exercício 6 - Construir as funções PrimFila() e UltFila() que retorna, respectivamente, o primeiro e o último elemento de uma fila circular")
        print("Exercício 7 - Retirar 3 elementos de uma fila de tamanho n, realizar o deslocamento e inserir 2 elementos às posições liberadas")
        print("Exercício 8 - Filas qpar, qimpar e Pilha s")
        print("Exercício 9 - Implementar 3 filas em um único vetor.")
        print("Exercício 10 - Fila com Prioridade 0 e 1")
        print("Exercício 11 - Implementar 2 filas cuja inserção deverá considerar o balanceamento da carga de cada uma.")
        Menu.Opções()

    def Opções():
        print("")
        o = int(input("Exercício: "))
        print("-----------------------------------------------------------------------------")
        print("")
        if(o == 0):
            sys.exit()
        if(o == 1):
            ex1=Ex1()
            ex1.Insere_Remove_AddPares()
        if(o == 2):
            ex2=Ex2()
            ex2.Inverte_Pilha()
        if(o == 3):
            ex3=Ex3()
            ex3.Eliminar_Impares()
        if(o == 4):
            ex4=Ex4()
            ex4.Eliminar_Negativos()
        if(o == 5):
            ex5=Ex5()
            ex5.Inverter()
        if(o == 6):
            ex6=Ex6()
            ex6.PrimFila()
            print("Função UltFila: ")
            ex6.UltFila()
        if(o == 7):
            ex7=Ex7()
            ex7.Rem3_Add2()
        if(o == 8):
            ex8=Ex8()
            ex8.qpar_qimpar_ps()
        if(o == 9):
            ex9=Ex9()
            ex9.Vetor()
        if(o == 10):
            ex10=Ex10()
            ex10.Prioridade()
        if(o == 11):
            ex11=Ex11()
        Menu.Painel()

class Ex1(Fila):
    def Insere_Remove_AddPares(self):
        pares=[]
        self.Inserir_Int()
        print("")
        self.Print()
        for j in range(0,len(self.fila)):
            self.Remover()
            if(self.p%2 == 0):
                pares.append(self.p)
        for h in range(0,len(pares)):
            a=pares.pop()
            self.fila.insert(0,a)
        print("Fila com Pares: ", self.fila)

class Ex2(Fila):
    def Inverte_Pilha(self):
        pilha=[]
        self.Inserir_Input()
        print("")
        self.Print()
        for i in range(0,len(self.fila)):
            a=self.fila.pop()
            pilha.append(a)
        print("Fila Invertida: ", pilha)

class Ex3(Fila):
    def Eliminar_Impares(self):
        aux=[]
        self.Inserir_Int()
        print("")
        self.Print()
        for h in range(0,len(self.fila)):
            a=self.fila.pop(0)
            if(a%2 == 0):
                aux.append(a)
        print("Fila sem os impares: ", aux)

class Ex4(Fila):
    def Eliminar_Negativos(self):
        aux=[]
        self.Inserir_Int()
        print("")
        self.Print()
        for j in range(0,len(self.fila)):
            self.Remover()
            if(self.p >= 0):
                aux.append(self.p)
        print("Fila sem os negativos: ",aux)

class Ex5(Fila):
    def Inverter(self):
        pilha=[]
        self.Inserir_Input()
        print("")
        self.Print()
        for i in range(0,len(self.fila)):
            a=self.fila.pop()
            pilha.append(a)
        print("Fila Invertida: ", pilha)

class Ex6(Fila):
    def PrimFila(self):
        for i in range(0,10):
            a=randrange(0,9)
            self.fila.append(a)
        print("Fila gerada aleatoriamente: ",self.fila)
        print("")
        print("Função PrimFila:")
        print("Primeiro elemento dessa fila: ",self.fila[0])
        print("")

    def UltFila(self):
        print("Ultimo elemento desse fila: ",self.fila[len(self.fila)-1])

class Ex7(Fila):
    def Rem3_Add2(self):
        self.Inserir_Input()
        print("")
        self.Print()
        print("")
        print("Removendo 3 elementos... ")
        for i in range(0,3):
            self.Remover()
        print("")
        print("Adicione 2 elementos: ")
        for i in range(1,3):
            e=input("{}º Elemento a ser adicionado: ".format(i))
            self.fila.append(e)
        print("")
        self.Print()

class Ex8(Fila):
    def qpar_qimpar_ps(self):
        qpar=[]
        qimpar=[]
        s=[]
        a=1
        print("Digite o nº [0] para parar de digitar numeros")
        while(a!=0):
            a=int(input("Número: "))
            if(a%2==0 and a!=0):
                qpar.append(a)
            if(a%2==1):
                qimpar.append(a)
        if(len(qpar)>len(qimpar)):
            m=len(qpar)
        else:
            m=len(qimpar)
        for i in range(0,m):
            if(len(qpar) > 0):
                n=qpar.pop(0)
                s.append(n)
            if(len(qimpar) > 0):
                qimpar.pop(0)
                if(len(s)!=0):
                    s.pop()
        print("Pilha S: ",s)

class Ex9(Fila):
    def Vetor(self):
        vetor=[]
        for i in range(0,3):
            self.fila=[]
            n=int(input("Quantos elementos você deseja inserir à fila no indice {}: ".format(i)))
            for j in range(1,n+1):
                e=input("{}° Elemento a ser adicionado: ".format(j))
                self.fila.append(e)
            vetor.append(self.fila)
            print("")
        print("Vetor: ",vetor)

class Ex10(Fila):
    def Prioridade(self):
        p0=[]
        p1=[]
        n=int(input("Qual o tamanho da fila? "))
        for i in range(1,n+1):
            e=input("{}° Elemento: ".format(i))
            p=int(input("Prioridade do {}º Elemento [0/1]: ".format(i)))
            if(p == 0):
                p0.append(e)
            elif(p == 1):
                p1.append(e)
        self.fila=p0+p1
        print("Fila com ordem de prioridade: ",self.fila)

class Ex11(Fila):
    def __init__(self):
        self.fila1=[]
        self.fila2=[]
        n1=int(input("Tamanho da 1ª Fila: "))
        for i in range(1,n1+1):
            e=input("{}º Elemento: ".format(i))
            self.fila1.append(e)
        print("")
        n2=int(input("Tamanho da 2ª Fila: "))
        for i in range(1,n2+1):
            e=input("{}º Elemento: ".format(i))
            self.fila2.append(e)
        print("")
        print("Fila 1: ",self.fila1)
        print("Fila 2: ",self.fila2)
        print("")
        self.Add_Rem()

    def Add_Rem(self):
        if(len(self.fila1) == len(self.fila2)):
            print("As Filas estão balanceadas!")
            print("Fila 1: ",self.fila1)
            print("Fila 2: ",self.fila2)
            Menu.Painel()
        a=input("Você deseja adicionar[a] ou remover[r] elementos: ")
        if(a == 'a'):
            if(len(self.fila1)<len(self.fila2)):
                e=input("Elemento a ser adicionado: ")
                self.fila1.append(e)
            elif(len(self.fila2)<len(self.fila1)):
                e=input("Elemento a ser adicionado: ")
                self.fila2.append(e)
            print("")
            print("Fila 1: ",self.fila1)
            print("Fila 2: ",self.fila2)
            print("")
            self.Add_Rem()
        elif(a == 'r'):
            if(len(self.fila1)>len(self.fila2)):
                self.fila1.pop(0)
            elif(len(self.fila2)>len(self.fila1)):
                self.fila2.pop(0)
            print("")
            print("Fila 1: ",self.fila1)
            print("Fila 2: ",self.fila2)
            print("")
            self.Add_Rem()
        
            
    
Menu.Painel()
            
