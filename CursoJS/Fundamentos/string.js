const a = "Osmar123"

console.log(a.charAt(4)) //retorna o caractere na posição em parenteses
console.log(a.indexOf("1")) //retorna a posição do caractere em parenteses

console.log(a.substring(1)) //forma uma substring excluindo os caracteres que estiverem antes da posição indicada
console.log(a.substring(5, 8)) //forma uma substring a partir da primeira posição indicada até a ultima posição indicada. A ultima posição não entra na substring

console.log("aaaaaaaa ".concat(a).concat("!")) //concatena strings
console.log("aaaaaaaa "+a+"!") //o + também concatena
console.log("3"+2) //ao usar o + utilizando um numero no formato string e outro no formato number os numeros são concatenados

console.log(a.replace(3, "4")) //substitui o caracter 3 na string por 4
console.log(a.replace(/\d/g, "a")) //substitui todos os numeros pelo caracter indicado
console.log(a.replace(/\w/g, "a")) //substitui todos os caracteres pelo caractere indicado

console.log("Osmar,Alexandre,Amanda".split(",")) //gera um array com separando cada palavra na ,

//Template
const nome = "Osmar"
const template = `
Olá,
${nome}!`
console.log(template)
console.log(`1 + 1 = ${1 + 1}`)

const up = texto => texto.toUpperCase()
console.log(`Ei... ${up("cuidado")}!`)