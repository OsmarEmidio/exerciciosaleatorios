'''
data = 28/02/2018
1. Projete um programa que leia quanto dinheiro uma pessoa tem na carteira
 e mostre quantos dólares ela pode comprar.
'''


#considere U$$ 1.00 = R$3,37

#recebe um valor do usuário e converte para real.

#entrada de dados
reais=float(input('Você possui quantos reais na carteira? R$ '))
d=float(input('Cotação do Dolar: U$$ '))
#conversão de reia
dolares = reais/d

'''
cada parametro dentro do metodo .format , será mostrado
atravez do par de chaves do corpo do texto
'''
#saida de mais de uma variavel com o . format
print('Com seus R$ {:.2f}, poderá comprar U$$ {:.2f} dolares'.format(reais, dolares))


'''
Alterações:
a. Peça para o usuario inserir a cotação do dolar atual, mostre no final, A quantidade de dolares comprados,
o valor em reais na sua carteira e a cotação atual.
b. Peça para o usuario inserir a cotação do dolar atual, mostre no final, A quantidade de dolares comprados,
o valor em reais na sua carteira e a cotação atual, utilizando o metodo .format.

'''
