console.log('01)', '1' == 1) //igual em valor
console.log('02)', '1' === 1) //estritamente igual (vê o tipo e o valor)
console.log('03)', '1' != 1) //diferente em valor
console.log('04)', '1' !== 1) //estritamente diferente

console.log('05)', 3 < 2) //menor que
console.log('06)', 3 > 2) //maior que
console.log('07)', 3 <= 2) //menor ou igual
console.log('08)', 3 >= 2) //maior ou igual

const d1 = new Date(0)
const d2 = new Date(0)
console.log('09)', d1 === d2) //é falso pois ele analisa o endereço da variavel e não o valor em si
console.log('10)', d1 == d2)
console.log('11)', d1.getTime() === d2.getTime())

console.log('12)', undefined == null)
console.log('13)', undefined === null)

/*Operadores lógicos
# || -> ou
# && -> e
# !!(op1 ^ op2) -> bitwise xor
# ! -> negação
*/
