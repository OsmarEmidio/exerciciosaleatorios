'''
8. Faça	um programa que recebe a idade de  um  nadador e
classifique-o numa das seguintes categorias:

• Adulto  (idade  >=  18);  
• Juvenil (idade >=  14  e idade <  18);  
• Infantil (idade >=9 e idade < 14);  
• Mirim  (Idade < 9).

 Operadores lógicos (and, or, not)

variaveis            and      or
 A       B          A . B    A + B

False   False      False    False
False   True       False    True
True    False      False    True
True    True       True     True

not

 A      not A
False   True
True    False

   --- DICAS ABAIXO:

if <condição>:
    comandos
elif <condição>:
    comandos
else:
    comandos

 '''

i=int(input("Idade do Nadador: "))
if(i>=18):
    print("O nadador com a idade {} compete na categoria Adulto".format(i))
elif(i>=14):
    print("O nadador com a idade {} compete na categoria Juvenil".format(i))
elif(i>=9):
    print("O nadador com a idade {} compete na categoria Infantil".format(i))
else:
    print("O nadador com a idade {} compete na categoria Mirim".format(i))

'''
Alterações:
a. Mostrar também na tela de saída a idade do nadador.
print ('O nadador com a idade {} é um "categoria' .format(idade)) #dica

'''
