//let e const
{
    var a = 2
    let b = 3
    console.log(b)
}
console.log(a)

//Template
const produto = 'IPad'
console.log(`Produto : ${produto}`)

//Destructuring
const [l, e, ...tras] = "Osmar"
console.log(l, e, tras)

const [x, , y] = [1, 2, 3]
console.log(x, y)

const {nome: n, idade} = {nome: 'Osmar', idade: 21}
console.log(n, idade)