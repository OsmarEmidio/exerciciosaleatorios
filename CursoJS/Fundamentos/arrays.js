const valores = [7, 8, 9.9, 10]
console.log(valores[0], valores[3])
console.log(valores[4]) //js não dá erro de out of range

valores[4] = 11
console.log(valores)
console.log(valores.length) //tamanho do array

valores.push({id: 3}, false, null, "teste") //adiciona elementos ao array
console.log(valores)

console.log(valores.pop()) //retorna e retira o ultimo elemento do array
delete valores[0] //deleta um elemento do array na posição indicada
console.log(valores)