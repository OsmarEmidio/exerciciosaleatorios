function crescimento(altura1, tc1, altura2, tc2){
    let anos = 0
    if(altura1 == altura2){
        console.log("As crianças tem o mesmo tamanho")
    }
    else if(altura1 > altura2){
        if(tc1 > tc2){
            console.log("A criança 1 será sempre maior que a 2")
        }
        else{
            while(altura1 > altura2){
                altura1 = altura1 * tc1
                altura2 = altura2 * tc2
                anos++
            }
            console.log(`A criança 1 é maior, mas a criança 2 irá ultrapassa-la em ${anos} ano(s).`)
        }
    }
    else{
        if(tc2 > tc1){
            console.log("A criança 2 será sempre maior que a 1")
        }
        else{
            while(altura2 > altura1){
                altura1 = altura1 * tc1
                altura2 = altura2 * tc2
                anos++
            }
            console.log(`A criança 2 é maior, mas a criança 1 irá ultrapassa-la em ${anos} ano(s).`)
        }
    }
}

crescimento(150, 2, 130, 4)