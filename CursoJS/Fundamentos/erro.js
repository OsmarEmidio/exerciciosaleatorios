function tratarErroELancar(erro){
    throw new Error('Deu ruim')
}

function caixaAlta(p){
    try{
        console.log(p.palavra.toUpperCase() + "!!!!")
    }
    catch (e){
        tratarErroELancar(e)
    }
    finally{
        console.log('finally sempre aparece independente de ter erro ou não')
    }
}

const p = {palavra: "aaaaaaaaaaaa"}
caixaAlta(p)