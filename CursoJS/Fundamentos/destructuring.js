//destructuring object
const pessoa = {
    nome: "Ana",
    idade: 18,
    endereco: {
        logradouro: "Rua AAAAAAAAAAA",
        numero: 15
    }
}

const {nome, idade} = pessoa
console.log(nome, idade)

const {nome: n, idade: i = 2} = pessoa //define um valor padrão
console.log(n,i)

const {endereco: {logradouro, numero, cep}} = pessoa
console.log(logradouro, numero, cep)

//destructuring array
const a = [10, 7, 8]

const [ ,n2, ] = a
console.log(n2)
const [n1, , n3] = a
console.log(n1, n3)