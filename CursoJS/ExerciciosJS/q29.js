function vetor_10_20(vetor){
    let estao = 0
    let nao_estao = 0
    for (let i in vetor){
        if(vetor[i] >= 10 && vetor[i] <= 20){
            estao++
        }
        else{
            nao_estao++
        }
    }
    console.log(`${estao} estão no intervalo e ${nao_estao} não estão`)
}

vetor_10_20([1,2,3,4,5,6,7,8,9, 10, 15, 20])