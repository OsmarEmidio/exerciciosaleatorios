''' 10. 
Escreva	 um programa que leia as medidas dos lados de um triângulo e escreva
se ele é Equilátero, Isósceles ou Escaleno. Sendo que:		
− Triângulo	Equilátero:	possui	os	3	lados	iguais.		
− Triângulo	Isóscele:	possui	2	lados	iguais.		
− Triângulo	Escaleno:	possui	3	lados	diferentes.	
   '''

l1=float(input("Medida do Primeiro Lado: "))
l2=float(input("Medida do Segundo Lado: "))
l3=float(input("Medida do Terceiro Lado: "))
if(l1==l2):
    if(l2==l3):
        print("É um Triangulo Equilatero")
    else:
        print("É um Triangulo Isóscele")
elif(l1==l3):
    if(l3==l2):
        print("É um Triangulo Equilatero")
    else:
        print("É um Triangulo Isóscele")
elif(l2==l3):
    if(l2==l1):
        print("É um Triangulo Equilatero")
    else:
        print("É um Triangulo Isóscele")
else:
    print("É um Triangulo Escaleno")
