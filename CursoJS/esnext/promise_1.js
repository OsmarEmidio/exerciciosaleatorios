let a = 1
console.log(a)

let p = new Promise(function(cumprirPromessa){
    cumprirPromessa(['Ana', 'Amanda', 'Vitu', 'Alexandre'])//pode ser passado objeto, função e qualquer outro elemento como parâmetro
})

p.then(valor => valor[0])
    .then(primeiroNome => primeiroNome[0])
    //.then(primeiraLetra => primeiraLetra.toLowerCase())
    .then(letraMinuscula => console.log(letraMinuscula))