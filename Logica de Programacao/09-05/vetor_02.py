
'''
Programa que cria um vetor de 5 numeros inteiros e  monstre-os
'''

# inicio do programa

#criando um vetor/lista vazia
vetor = []
#criando uma estrutura para preencher o vetor/lista
for i in range(1,6):
    num=int(input('Digite o numero %i de 5: '%i))
    vetor.append(num) #metodo append(variavel) adiciona dinamicamente
                      #variaveis a um(a) vetor/lista

#imprimir o vetor
print(vetor)

'''
a- aalterar o programa para que o tamanho do vetor sejá criado pelo usuário.
b- Faça um programa que leia 4 notas, mostre as notas e a média na tela.
c- Faça um Programa que leia 20 números inteiros e armazene-o em um vetor.
Armazene os números pares no vetor PAR e os números impares no vetor IMPAR.
Imprima os três Vetores.
