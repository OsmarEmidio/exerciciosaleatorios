//verdadeiro
const verdadeiro = true
console.log(!!3)
console.log(!!-1)
console.log(!!"qualquer coisa")
console.log(!![])
console.log(!!{})
console.log(!!Infinity)
console.log(!!verdadeiro)
console.log("")
//falso
const falso = false
console.log(!!0)
console.log(!!"")
console.log(!!null)
console.log(!!NaN)
console.log(!!undefined)
console.log(!verdadeiro)
console.log(!!falso)
console.log("")

console.log('' || null || 0 || "nome")