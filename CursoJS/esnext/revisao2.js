//Arrow Function
const soma = (a, b) => a + b
console.log(soma(2,3))

//Arrow Function (this)
const lexico1 = () => console.log(this === exports)
const lexico2 = lexico1.bind({})
lexico1()
lexico2()

//parametro default
function log(texto = 'Node'){
    console.log(texto)
}
log()
log(undefined)
log('Sou mais forte')

//Operador rest
function total(...numeros){
    let t = 0
    numeros.forEach(n => t += n);
    return t
}
console.log(total(1,2,3,4))