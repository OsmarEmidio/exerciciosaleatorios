//Função sem retorno
function imprimirSoma(a, b){
    console.log(a + b)
}

imprimirSoma(2, 3)
imprimirSoma(2)
imprimirSoma(2, 3, 4, 5, 6)
imprimirSoma()

//Função com retorno
function soma(a, b = 0){ //valor padrão pro parametro b
    return a+b
}

console.log(soma(2, 3))
console.log(soma(2))

//Armazenando uma função em uma variavel
const impSoma = function(a,b){
    console.log(a + b)
}

impSoma(2, 3)

//Armazenando com uma função arrow em uma variavel
const arrowsoma = (a,b) => {
    return a + b
}

console.log(arrowsoma(2,3))

//retorno implicito
const subtracao = (a, b) => a - b
console.log(subtracao(2, 3))
