const pessoa = {
    saudacao: "Bom Dia!",
    falar() {
        console.log(this.saudacao)
    }
}

pessoa.falar()
const falar = pessoa.falar
falar() //conflito entre paradigmas: funcional e oo

const falar2 = pessoa.falar.bind(pessoa)
falar2()

console.log("------------------------------------------")

function Pessoa2(){
    this.idade = 0
    const self = this
    setInterval(function(){
        /*this*/self.idade++
        console.log(/*this*/self.idade)
    }/*.bind(this)*/, 1000)
}

new Pessoa2