'''
data = 28/02/2018
3. Faça um algoritmo que leia o salário de um funcionário 
e mostre seu novo salário, com 15% de aumento.
		
'''


#calcular a procentagem de 15% referente ao salario de entrada.
#utilizar uma varivel temporaria para receber a porcentagem de 15%

salario_inicial=float(input('Informe seu salário inicial : R$ '))
salario_final= salario_inicial + (salario_inicial*0.10)

print('Seu salario inicial é de R$ {}\n\n' .format(salario_inicial)) 
print('Seu salario final é de R$ {}' .format(salario_final))

'''
Alterações:
a. Na tela de saída de dados, mostre o salario antigo,
 a porcentagem de aumente e o salario atual, utilizando o metodo .format.

'''
