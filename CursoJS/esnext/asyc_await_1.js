function esperarPor(tempo = 2000){
    return new Promise(function(resolve){
        setTimeout(() => {
            console.log('Executando Promise...')
            resolve('Vish')
        }, tempo)
    })
}

esperarPor(2000)
    .then(esperarPor)
    .then(esperarPor)
    .then(esperarPor)

async function executar(){
    await esperarPor()
    console.log('Assync/Await...')
    await esperarPor()
    console.log('Assync/Await...')
    await esperarPor()
    console.log('Assync/Await...')
}