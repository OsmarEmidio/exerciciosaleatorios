//Object.values/Object.entries
const obj = {a:1, b:2, c:3}
console.log(Object.values(obj))
console.log(Object.entries(obj))

//Melhorias na notação literal
const nome = 'Osmar'
const obj2 = {
    nome,
    ola() {
        return 'Olá'
    }
}
console.log(obj2.nome, obj2.ola())

//Class
class Animal{}
class Cachorro extends Animal {
    falar(){
        return 'Au Au!'
    }
}
console.log(new Cachorro().falar())