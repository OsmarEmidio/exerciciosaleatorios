for (var i = 0; i < 10; i++){
    console.log(i)
}

console.log("i = ", i)
console.log("---------------------------------------------")
for (let j = 0; j < 10; j++) {
    console.log(j)
}

//console.log("j = ", j)
//o let só vale dentro do bloco
console.log("---------------------------------------------")

const funcs = []

for (var v = 0; v <10; v++){
    funcs.push(function(){
        console.log(v)
    })
}

funcs[2]()
funcs[8]()
//o var só retorna o valor final de v na função
console.log("---------------------------------------------")

const funcs2 = []

for (let l = 0; l < 10; l++){
    funcs2.push(function(){
        console.log(l)
    })
}

funcs2[2]()
funcs2[8]()
//o let salva e retorna cada valor de l dentro da função