//Factory Simples
function criarPessoa(){
    return {
        nome: "Ana",
        idade: 18
    }
}

console.log(criarPessoa())

//Exercício

function criarProduto(nome, preco, desconto = 0){
    return {
        produto: nome,
        preco: preco,
        desconto: desconto
    }
}

const biscoito = criarProduto("biscoito", 2)
console.log(biscoito)