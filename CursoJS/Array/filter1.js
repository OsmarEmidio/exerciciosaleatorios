const produtos = [
    {nome: 'Notebook', preco: 2499, fragil: true},
    {nome: 'Ipad', preco: 2000, fragil: true},
    {nome: 'Copo de Vidro', preco: 9, fragil: true},
    {nome: 'Copo de Plastico', preco: 5, fragil: false}
]

console.log(produtos.filter(function(p){
    return p.preco > 1000
})) //se retornar um false o elemento não é incluído na nova lista

const caro = produto => produto.preco >= 500
const fragil = produto => produto.fragil

console.log(produtos.filter(caro).filter(fragil))