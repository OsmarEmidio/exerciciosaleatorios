function impares(inicio = 0, fim = 100){
    let vetor = []
    if(inicio > fim){
        inicio = fim + inicio
        fim = inicio - fim
        inicio = inicio - fim
    }
    while(inicio+1 < fim){
        if(inicio%2 == 0){
            inicio+=1
            vetor.push(inicio)
        }
        inicio+=2
        vetor.push(inicio)
    }
    console.log("Números: " + vetor)
}

impares(1,10)