'''
def função(arg1, arg2, ... , argn):
    script

    return val1, val2, ... , valn

def --> declara que estamos construindo uma funçao
função --> bome da função
script --> Código da função
return --> Indica o fim da função e o que estará sendo retornado

val1, val2, ... , valn --> Valores retornados

4.Escreva uma função que retorne o fatorial de um numero
digitado pelo usuario
'''
# declaração da função soma

import math
'''
def fatorial(num1):
    script
    return fat 
'''
#chamando a função dentro do print


print(math.factorial(5)) # retorna o fatorial do numero 5
                      # 5! = 5x4x3x2x1= 120


'''    Alterações:

a. Criar a função fatorial utilizando o laço 'while'
b. Modificar o item a, para uma função fatorial
utilizando o laço 'for'
'''
