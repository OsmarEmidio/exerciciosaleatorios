'''
9. Escreva um programa para ler 2 valores inteiros e
escrever o maior deles. Considere que o usuário	não
informará valores iguais.
	
--- Dicas:
- Operadores aritméticos utilizados em Python:
    + → soma
    – → subtração
    * → multiplicação
    / → divisão
    // → divisão de inteiros (quociente da divisão)
    ** → potenciação
    % → módulo (resto da divisão)
- Operadores de Comparação:
Operador 	Descrição 	Exemplo 
< 	Menor que  		a < 10 
<= 	Menor ou igual 		b  <= 5 
> 	Maior que  		c  > 2 
>= 	Maior ou igual 		d  >= 8 
== 	Igual 	                e == 5 
!= 	Diferente 	       f  != 12 
'''

# recebe um primeiro número digitado
num1 = int(input("Digite o primeiro número:"))
# recebe um primeiro número digitado
num2 = int(input("Digite o segundo número:"))

num3=int(input('digite o terceiro numero'))

# verifica se num1 é maior os outros valores
if (num1 > num2 and num1>num3):
    if(num2>num3): # verifica se num2 é maior que num3
        print('a ordem crescente é {}, {}, {}'.format(num3,num2,num1))
    else:
        print('a ordem crescente é {}, {}, {}'.format(num2,num3,num1))
# verifica se num2 é maior os outros valores       
elif(num2 > num1 and num2>num3):
    if(num1>num3):# verifica se num1 é maior que num3
        print('a ordem crescente é {}, {}, {}'.format(num3,num1,num2))
    else:
        print('a ordem crescente é {}, {}, {}'.format(num1,num3,num2))
    
else: # num3 é maior os outros valores  
    if(num1>num2): # verifica se num1 é maior que num2
        print('a ordem crescente é {}, {}, {}'.format(num2,num1,num3))
    else:
        print('a ordem crescente é {}, {}, {}'.format(num1,num2,num3))
    
''' Alterações:
d. Escreva um programa para ler 3 valores inteiros (considere que não serão lidos valores iguais) e
escrevê-los em ordem crescente.

   --- DICAS ABAIXO:

if <condição>:
    comandos
elif <condição>:
    comandos
else:
    comandos
-------------
um If dentro de outro if

if <condição>:
    if<condição>:
        comandos
elif <condição>:
    if<condição>:
        comandos
else:
    if<condição>:
        comandos
'''
