//map serve para transformar os elementos de um array de um tipo para outro. Gera outro array

const n = [1, 2, 3, 4, 5]

//For com propósito
let resultado = n.map(function(e){
    return e*2
})
console.log(resultado)

const soma10 = e => e + 10
const triplo = e => e * 3
const reais = e => `R$ ${parseFloat(e).toFixed(2).replace('.', ',')}`
resultado = n.map(soma10).map(triplo).map(reais)
console.log(resultado)