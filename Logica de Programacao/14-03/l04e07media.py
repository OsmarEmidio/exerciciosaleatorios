﻿'''
7. Elabore o programa que leia tres notas, mostre a média aritmética das notas
e verfique se a media for maior ou igual a 6, imprima aluno aprovado, caso contrario,
imprima aluno reprovado.

--- Dicas:
- Operadores aritméticos utilizados em Python:
    + → soma
    – → subtração
    * → multiplicação
    / → divisão
    // → divisão de inteiros (quociente da divisão)
    ** → potenciação
    % → módulo (resto da divisão)

- Operadores de Comparação:
Operador 	Descrição 	Exemplo 
< 	Menor que  		a < 10 
<= 	Menor ou igual 		b  <= 5 
> 	Maior que  		c  > 2 
>= 	Maior ou igual 		d  >= 8 
== 	Igual 	                e == 5 
!= 	Diferente 	       f  != 12 


 Operadores lógicos (and, or, not)

variaveis            and      or
 A       B          A . B    A + B

False   False      False    False
False   True       False    True
True    False      False    True
True    True       True     True

not

 A      not A
False   True
True    False

   --- DICAS ABAIXO:

if <condição>:
    comandos
elif <condição>:
    comandos
else:
    comandos

'''

n1=float(input("Primeira Nota: "))
n2=float(input("Segunda Nota: "))
n3=float(input("Terceira Nota: "))
n4=float(input("Quarta Nota: "))
f=int(input("Numero de Faltas: "))
m=(n1+n2+n3+n4)/4
print("Sua media é de {:.2f}".format(m))
if(m>=7 and f<=36):
    print("Aluno Aprovado")
elif(m>=5 and f<=36):
    print("Aluno de Exame Final")
else:
    print("Aluno Reprovado")

'''
Alterações:
a- Faça um programa que	receba quatro notas, calcule e mostre a	média aritmética
entre elas, caso a media aritmética seja:
    - Media maior ou  igual a 7– ALUNO APROVADO
    - Media maior ou igual a 5 e menor que 7– ALUNO DE EXAME FINAL
    - Media menor a 5– ALUNO REPROVADO

b- Criar um programa que calcule a média final das notas de um aluno. O	programa deve
ler as notas dos 4 bimestres e o número de faltas. Em seguida deve calcular a média
artmética das notas e imprimir umas das seguintes mensagens:

- ‘aprovado’ para média entre 7.0 e 10.0 e número de faltas até 36; 
- ‘Exame final’ - média de 40 à 70 e número de faltas até	36;
- ‘Reprovado’ - média abaixo de 40 ou número de faltas maior que 36;

'''
