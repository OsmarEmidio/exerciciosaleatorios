'''
5. As maçãs custam R$0.30 cada se foem compradas menos que uma duzia, e R$0.25 se forem compradas pelo menos 12.
Escreva um programa que leia o numero de maçãs compradas, calcule e escreva o valor total da compra.
'''

n=int(input("Numero de Maçãs Compradas: "))
if(n>=12):
    v1=n*0.25
    print("Valor total da compra: R${:.2f}".format(v1))
else:
    v2=n*0.3
    print("Valor total da compra: R${:.2f}".format(v2))
