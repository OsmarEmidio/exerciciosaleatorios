const fabricantes = ["Mercedes", "Audi", "BMW", "Ferrari"]

//A função forEach passa como parâmetro para a função que ele retorna primeiro o item da lista e depois o indice

function imprimir(nome, indice) {
    console.log(`${indice + 1}. ${nome}`)
}

fabricantes.forEach(imprimir)