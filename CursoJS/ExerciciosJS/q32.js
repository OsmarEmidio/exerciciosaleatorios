function media_vetor(vetor){
    let soma_total = 0
    let media = 0
    for (i in vetor){
        soma_total += vetor[i]
    }
    media = soma_total/vetor.length
    return media
}

console.log(media_vetor([1,2,3]))