/*06) Elabore duas funções que recebem três parâmetros: capital inicial, taxa de juros e tempo de aplicação. A
primeira função retornará o montante da aplicação financeira sob o regime de juros simples e a segunda
retornará o valor da aplicação sob o regime de juros compostos.*/

function jurosSimples (valor_inicial, juros, meses){
    let total = valor_inicial
    for(let i = meses; i != 0; i--){
        total += valor_inicial * juros
    }
    return total
}

console.log(jurosSimples(1000, 0.1, 3))

function jurosComposto(valor_inicial, juros, meses){
    let total = valor_inicial
    for(let i = meses; i != 0; i--){
        total += total * juros
    }
    return total
}

console.log(jurosComposto(1000, 0.1, 7).toFixed(2))
