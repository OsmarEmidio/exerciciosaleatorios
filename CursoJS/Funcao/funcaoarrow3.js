let comparaComThis = function(parametro) {
    console.log(this === parametro)
}
comparaComThis(global)
comparaComThis(this)

const obj = {}
comparaComThis = comparaComThis.bind(obj)
comparaComThis(global)
comparaComThis(obj)

console.log("-----------------------------------------------------")

let comparaComThisArrow = parametro => console.log(this === parametro)
comparaComThisArrow(global)
comparaComThisArrow(this)
comparaComThisArrow(module.exports)

comparaComThisArrow = comparaComThisArrow.bind(obj)
comparaComThisArrow(obj)
comparaComThisArrow(this)