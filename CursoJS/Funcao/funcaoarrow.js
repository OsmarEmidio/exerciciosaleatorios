let dobro = function (a) {
    return 2 * a
}
console.log(dobro(2))

dobro = (a) => {
    return 2 * a
}
console.log(dobro(2))

dobro = a => 2 * a //return implícito
console.log(dobro(2))

console.log("-------------------------------------")

let ola = function () {
    return "Olá"
}
console.log(ola())

ola = () => "Olá"
console.log(ola())

ola = _ => "Olá" //possui um parametro, mas é ignorado
console.log(ola())