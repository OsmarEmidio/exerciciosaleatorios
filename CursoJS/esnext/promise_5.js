function funcionarOuNao(valor, chanceErro) {
    return new Promise((resolve, reject) => {
        try{
            //con.log('temp')
            if(Math.random() < chanceErro){
                reject('Ocorreu um Erro!')
            }else{
                resolve(valor)
            }
        } catch(e){
            reject(e)
        }
    })
}

funcionarOuNao('Testando', 0.5)
    .then(console.log)
    .catch(err => console.log(`Erro: ${err}`))
    .then(() => console.log('Fim!'))