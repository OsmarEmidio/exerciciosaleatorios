const pessoa = {
    nome: 'Rebeca',
    idade: 20,
    peso: 13
}

console.log(Object.keys(pessoa), 'Object.keys')
console.log(Object.values(pessoa), 'Object.values')
console.log(Object.entries(pessoa), 'Object.entries')

Object.entries(pessoa).forEach(element => {
    console.log(`${element[0]}: ${element[1]}`)    
});

Object.defineProperty(pessoa, 'dataNascimento', {
    enumerable: true,
    writable: false,
    value: '01/01/2019'
})
pessoa.dataNascimento = '01/01/1999'
console.log(pessoa.dataNascimento)

const dest = {a: 1}
const o1 = {b: 2}
const o2 = {c: 3, a: 4}
const obj = Object.assign(dest, o1, o2)
console.log(dest, 'Object.assign')

Object.freeze(pessoa)
pessoa.nome = "Ana"
console.log(pessoa)