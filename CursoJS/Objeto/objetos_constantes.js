//pessoa -> endereço de memoria -> {...}
const pessoa = {nome: 'Pedro'}
pessoa.nome = 'João'
console.log(pessoa)

//pessoa = {nome: 'Ana'} => Dá erro

Object.freeze(pessoa) //o objeto não poderá ser alterado

pessoa.nome = 'Maria'
pessoa.end = 'aaaaa'
delete pessoa.nome
console.log(pessoa)

const pessoaConstante = Object.freeze({ nome:'Pedro' })
pessoaConstante.nome = 'João'
console.log(pessoaConstante)