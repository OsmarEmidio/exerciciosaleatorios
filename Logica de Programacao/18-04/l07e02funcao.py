'''
def função(arg1, arg2, ... , argn):
    script

    return val1, val2, ... , valn

def --> declara que estamos construindo uma funçao
função --> bome da função
script --> Código da função
return --> Indica o fim da função e o que estará sendo retornado

val1, val2, ... , valn --> Valores retornados

2.Escreva uma função que recebe tres números e devolve a soma
'''
# declaração da função soma
def soma(num1, num2, num3):
    soma=num1+num2+ num3
    return soma #retorna dois valores 

#chamando a função dentro do print

print(soma(2,3, 4)) # a chamada da função deverá sempre ser utilizada após a criação da função


'''    Alterações:
a. Criar uma variavel para receber o valor retornado da função
b. Fazer com que o usuario entre com os valores de entrada (num1, num2, num3).
c. Criar uma função chamada maior(n1, n2, n3), a mesma deverá retornar o maior
numero inserido
d. Criar uma função chamada menor(n1, n2, n3), deverá retornar o menor número

'''
