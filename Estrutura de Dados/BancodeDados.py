import sqlite3
import sys
import os

conexao = sqlite3.connect('livraria.db')
cursor = conexao.cursor()

class Menu():
    def __init__(self):
        print("|======================== Tabelas ========================|")
        print("| 0 - Sair                                                |")
        print("| 1 - Clientes                                            |")
        print("| 2 - Funcionários                                        |")
        print("| 3 - Estoque                                             |")
        print("| 4 - Mostrar Todas as Tabelas                            |")
        print("|=========================================================|\n")
        self.o=int(input("-> "))
        print("")
        Menu.Tabelas(self)

    def Tabelas(self):
        if(self.o == 0):
            cursor.close()
            conexao.close()
            sys.exit()
        elif(self.o == 1):
            clear = lambda: os.system('cls')
            clear()
            Menu.TabelaCliente(self)
        elif(self.o == 2):
            clear = lambda: os.system('cls')
            clear()
            Menu.TabelaFuncionario(self)
        elif(self.o == 3):
            clear = lambda: os.system('cls')
            clear()
            Menu.TabelaEstoque(self)
        elif(self.o == 4):
            clear = lambda: os.system('cls')
            clear()
            print("==================== Tabela Clientes =====================")
            Cliente.Read(self)
            print("----------------------------------------------------------\n")
            print("================== Tabela Funcionarios ===================")
            Funcionario.Read(self)
            print("----------------------------------------------------------\n")
            print("=================== Tabela Estoque =======================")
            Estoque.Read(self)
            print("----------------------------------------------------------\n")
            Menu()
        else:
            clear = lambda: os.system('cls')
            clear()
            Menu()

    def TabelaCliente(self):
        print("|=================== Tabela Clientes =====================|")
        print("| 0 - Voltar                                              |")
        print("| 1 - Inserir Cliente                                     |")
        print("| 2 - Deletar Cliente                                     |")
        print("| 3 - Mostrar Tabela                                      |")
        print("| 4 - Atualizar Dados                                     |")
        print("|=========================================================|\n")
        self.oc = int(input("-> "))
        print("")
        Menu.MenuCliente(self)

    def MenuCliente(self):
        if (self.oc == 0):
            clear = lambda: os.system('cls')
            clear()
            Menu()
        if (self.oc == 1):
            Cliente.Inserir(self)
        elif (self.oc == 2):
            Cliente.Delete(self)
        elif (self.oc == 3):
            Cliente.Read(self)
        elif (self.oc == 4):
            Cliente.Update(self)
        clear = lambda: os.system('cls')
        clear()
        Menu.TabelaCliente(self)

    def TabelaFuncionario(self):
        print("|================= Tabela Funcionarios ===================|")
        print("| 0 - Voltar                                              |")
        print("| 1 - Inserir Funcionario                                 |")
        print("| 2 - Deletar Funcionario                                 |")
        print("| 3 - Mostrar Tabela                                      |")
        print("| 4 - Atualizar Dados                                     |")
        print("|=========================================================|\n")
        self.of = int(input("-> "))
        print("")
        Menu.MenuFuncionario(self)

    def MenuFuncionario(self):
        if (self.of == 0):
            clear = lambda: os.system('cls')
            clear()
            Menu()
        elif (self.of == 1):
            Funcionario.Inserir(self)
        elif (self.of == 2):
            Funcionario.Delete(self)
        elif (self.of == 3):
            Funcionario.Read(self)
        elif (self.of == 4):
            Funcionario.Update(self)
        clear = lambda: os.system('cls')
        clear()
        Menu.TabelaFuncionario(self)

    def TabelaEstoque(self):
        print("|=================== Tabela Estoque ======================|")
        print("| 0 - Voltar                                              |")
        print("| 1 - Inserir Produto                                     |")
        print("| 2 - Deletar Produto                                     |")
        print("| 3 - Mostrar Tabela                                      |")
        print("| 4 - Atualizar Dados                                     |")
        print("|=========================================================|\n")
        self.oe = int(input("-> "))
        print("")
        Menu.MenuEstoque(self)

    def MenuEstoque(self):
        if (self.oe == 0):
            clear = lambda: os.system('cls')
            clear()
            Menu()
        elif (self.oe == 1):
            Estoque.Inserir(self)
        elif (self.oe == 2):
            Estoque.Delete(self)
        elif (self.oe == 3):
            Estoque.Read(self)
        elif (self.oe == 4):
            Estoque.Update(self)
        clear = lambda: os.system('cls')
        clear()
        Menu.TabelaEstoque(self)

class Cliente():
    def __init__(self):
        cursor.execute('''CREATE TABLE if not exists TB_Cliente(
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    nome TEXT,
                    cpf TEXT,
                    idade INTEGER)''')
        conexao.commit()

    def Inserir(self):
        sql = "INSERT INTO TB_Cliente(nome, cpf, idade) values(?, ?, ?)"
        nome = input("Nome: ")
        cpf = input("CPF: ")
        idade = int(input("Idade: "))
        cursor.execute(sql, (nome, cpf, idade))
        conexao.commit()
        print("Cliente Adicionado!\n")

    def Delete(self):
        sql = "DELETE FROM TB_Cliente WHERE nome=?;"
        nome = input("Nome: ")
        cursor.execute(sql, (nome,))
        conexao.commit()
        print("Deletado com sucesso!\n")

    def Read(self):
        cursor.execute('''
        SELECT * FROM TB_Cliente;''')
        count = 0
        print("(ID, Nome, CPF, Idade)")
        for linha in cursor.fetchall():
            count += 1
            print(linha)
        print("")
        print("Número de clientes cadastrados: ", count)
        print("")

    def Update(self):
        nome = input("Nome do Cliente: ")
        print("")
        print("O que você deseja alterar? ")
        print("  0 - Tudo")
        print("  1 - Nome")
        print("  2 - CPF")
        print("  3 - Idade\n")
        a=int(input("-> "))
        print("")
        if(a==0):
            novo_cpf = input("Novo CPF: ")
            nova_idade = int(input("Nova Idade: "))
            novo_nome = input("Novo Nome: ")
            sql = '''UPDATE TB_Cliente SET cpf = ?, idade = ?, nome = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_cpf, nova_idade, novo_nome, nome))
            print("")
            print("Cliente Atualizado!\n")
        elif(a==1):
            novo_nome = input("Novo Nome: ")
            sql = '''UPDATE TB_Cliente SET nome = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_nome, nome))
            print("")
            print("Cliente Atualizado!\n")
        elif(a==2):
            novo_cpf = input("Novo CPF: ")
            sql = '''UPDATE TB_Cliente SET cpf = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_cpf, nome))
            print("")
            print("Cliente Atualizado!\n")
        elif(a==3):
            nova_idade = int(input("Nova Idade: "))
            sql = '''UPDATE TB_Cliente SET idade = ? WHERE nome = ?'''
            cursor.execute(sql, (nova_idade, nome))
            print("")
            print("Cliente Atualizado!\n")

class Funcionario():
    def __init__(self):
        cursor.execute('''CREATE TABLE if not exists TB_Funcionario(
                            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            nome TEXT,
                            cpf TEXT,
                            idade INTEGER,
                            telefone INTEGER,
                            email TEXT)''')
        conexao.commit()

    def Inserir(self):
        sql = "INSERT INTO TB_Funcionario(nome, cpf, idade, telefone, email) values(?, ?, ?, ?, ?)"
        nome = input("Nome: ")
        cpf = input("CPF: ")
        idade = int(input("Idade: "))
        email = input("E-mail: ")
        telefone = int(input("Telefone: "))
        cursor.execute(sql, (nome, cpf, idade, telefone, email))
        conexao.commit()
        print("Funcionario Adicionado!\n")

    def Delete(self):
        sql = "DELETE FROM TB_Funcionario WHERE nome=?;"
        nome = input("Nome: ")
        cursor.execute(sql, (nome,))
        conexao.commit()
        print("Deletado com sucesso!\n")

    def Read(self):
        cursor.execute('''
        SELECT * FROM TB_Funcionario;''')
        count = 0
        print("(ID, Nome, CPF, Idade, Telefone, E-mail)")
        for linha in cursor.fetchall():
            count += 1
            print(linha)
        print("")
        print("Número de funcionarios cadastrados: ", count)
        print("")

    def Update(self):
        nome = input("Nome do Funcionario: ")
        print("")
        print("O que você deseja alterar? ")
        print("  0 - Tudo")
        print("  1 - Nome")
        print("  2 - CPF")
        print("  3 - Idade")
        print("  4 - E-mail")
        print("  5 - Telefone")
        a=int(input("-> "))
        print("")
        if(a==0):
            novo_cpf = input("Novo CPF: ")
            nova_idade = int(input("Nova Idade: "))
            novo_nome = input("Novo Nome: ")
            novo_email = input("Novo E-mail: ")
            novo_telefone = int(input("Novo Telefone: "))
            sql = '''UPDATE TB_Funcionario SET cpf = ?, idade = ?, nome = ?, email = ?, telefone = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_cpf, nova_idade, novo_nome, novo_email, novo_telefone, nome))
            print("")
            print("Funcionario Atualizado!\n")
        elif(a==1):
            novo_nome = input("Novo Nome: ")
            sql = '''UPDATE TB_Funcionario SET nome = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_nome, nome))
            print("")
            print("Funcionario Atualizado!\n")
        elif(a==2):
            novo_cpf = input("Novo CPF: ")
            sql = '''UPDATE TB_Funcionario SET cpf = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_cpf, nome))
            print("")
            print("Funcionario Atualizado!\n")
        elif(a==3):
            nova_idade = int(input("Nova Idade: "))
            sql = '''UPDATE TB_Funcionario SET idade = ? WHERE nome = ?'''
            cursor.execute(sql, (nova_idade, nome))
            print("")
            print("Funcionario Atualizado!\n")
        elif(a==4):
            novo_email = input("Novo E-mail: ")
            sql = '''UPDATE TB_Funcionario SET email = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_email, nome))
            print("")
            print("Funcionario Atualizado!\n")
        elif(a==5):
            novo_telefone = int(input("Novo Telefone: "))
            sql = ''' UPDATE TB_Funcionario SET telefone = ? WHERE nome = ?'''
            cursor.execute(sql, (novo_telefone, nome))
            print("")
            print("Funcionario Atualizado!\n")

class Estoque():
    def __init__(self):
        cursor.execute('''CREATE TABLE if not exists TB_Estoque(
                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        produto TEXT,
                        quantidade INTEGER)''')
        conexao.commit()

    def Inserir(self):
        sql = "INSERT INTO TB_Estoque(produto, quantidade) values(?, ?)"
        produto = input("Produto: ")
        quantidade = int(input("Quantidade: "))
        cursor.execute(sql, (produto, quantidade))
        conexao.commit()
        print("Produto Adicionado!\n")

    def Delete(self):
        sql = "DELETE FROM TB_Estoque WHERE produto=?;"
        produto = input("Produto: ")
        cursor.execute(sql, (produto,))
        conexao.commit()
        print("Deletado com sucesso!\n")

    def Read(self):
        cursor.execute('''
        SELECT * FROM TB_Estoque;''')
        count = 0
        print("(ID, Produto, Quantidade)")
        for linha in cursor.fetchall():
            count += 1
            print(linha)
        print("")
        print("Número de funcionarios cadastrados: ", count)
        print("")

    def Update(self):
        produto = input("Nome do Produto: ")
        print("")
        nova_quantidade=int(input("Quantidade: "))
        sql = '''UPDATE TB_Estoque SET quantidade = ? WHERE produto = ?'''
        cursor.execute(sql, (nova_quantidade, produto))
        print("")
        print("Produto Atualizado!\n")

if __name__ == '__main__':
    Cliente()
    Funcionario()
    Estoque()
    Menu()
