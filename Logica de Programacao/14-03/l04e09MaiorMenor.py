'''
9. Escreva um programa para ler 2 valores inteiros e
escrever o maior deles. Considere que o usuário	não
informará valores iguais.
	
--- Dicas:
- Operadores aritméticos utilizados em Python:
    + → soma
    – → subtração
    * → multiplicação
    / → divisão
    // → divisão de inteiros (quociente da divisão)
    ** → potenciação
    % → módulo (resto da divisão)
- Operadores de Comparação:
Operador 	Descrição 	Exemplo 
< 	Menor que  		a < 10 
<= 	Menor ou igual 		b  <= 5 
> 	Maior que  		c  > 2 
>= 	Maior ou igual 		d  >= 8 
== 	Igual 	                e == 5 
!= 	Diferente 	       f  != 12 
'''

n1=int(input("Primeiro Numero: "))
n2=int(input("Segundo Numero: "))
n3=int(input("Terceiro Numero: "))
if(n1>n2 and n1>n3):
    if(n2>n3):
        print("O maior numero é {}, o intermediario é o {} e o menor é o {}".format(n1,n2,n3))
    else:
        print("O maior numero é {}, o intermediario é o {} e o menor é o {}".format(n1,n3,n2))
elif(n2>n3 and n2>n1):
    if(n1>n3):
        print("O maior numero é {}, o intermediario é o {} e o menor é o {}".format(n2,n1,n3))
    else:
        print("O maior numero é {}, o intermediario é o {} e o menor é o {}".format(n2,n3,n1))
else:
    if(n1>n2):
        print("O maior numero é {}, o intermediario é o {} e o menor é o {}".format(n3,n1,n2))
    else:
        print("O maior numero é {}, o intermediario é o {} e o menor é o {}".format(n3,n2,n1))

''' Alterações:
a. Mostre também na tela de saída os dois números digitados e o valor do maior e do menor número.
b. Escreva um programa para ler 3 valores inteiros e escrever o maior deles. Considere que o usuário 
não informará valores iguais.
c. Ler 3 valores e informar o maior e o menor número digitado
d. Escreva um programa para ler 3 valores inteiros (considere que não serão lidos valores iguais) e
escrevê-los em ordem crescente.

   --- DICAS ABAIXO:

if <condição>:
    comandos
elif <condição>:
    comandos
else:
    comandos
-------------
um If dentro de outro if

if <condição>:
    if<condição>:
        comandos
elif <condição>:
    if<condição>:
        comandos
else:
    if<condição>:
        comandos
'''
