// coleção finâmica de pares chave/valor
const produto = new Object
produto.nome = 'Cadeira'
produto['marca do produto'] = 'Generica'
produto.preco = 40

console.log(produto)
delete produto.preco
delete produto['marca do produto']
console.log(produto)

const carro = {
    modelo: 'A4',
    valor: 89000,
    proprietario: {
        nome: 'Raul',
        idade: 56
    } ,
    condutores: [{
        nome: 'Raul Junior',
        idade: 19
    }, {
        nome: 'Ana',
        idade: 42
    }],
    calcular_seguro: function(){ 
        //...
    }
}

carro.proprietario.idade = 57
carro.proprietario.nome = 'Raul Silva'
console.log(carro)

delete carro.calcular_seguro
delete carro.condutores
console.log(carro)