print("Equação de Primeiro Grau: ax+b=0")
a=float(input("Valor de a: "))
b=float(input("Valor de b: "))
if(a==0):
    print("Se a é igual a 0 não é uma equação de primeiro grau")
else:
    x=-b/a
    print("O valor de x é {}".format(x))
