function progressao_aritmetica(n, a1, r){
    let resultado = [a1]
    let contador = 0
    let v = 0
    while(n != 1+contador){
        v = resultado[contador] + r
        resultado.push(v)
        contador++
    }
    console.log("Progressão Aritmética: " + resultado)
}

function progressao_geometrica(n, a1, r){
    let resultado = [a1]
    let contador = 0
    let v = 0
    while(n != 1+contador){
        v = resultado[contador] * r
        resultado.push(v)
        contador++
    }
    console.log("Progressão Geométrica: " + resultado)
}

progressao_aritmetica(5,2,2)
progressao_geometrica(5,2,2)

