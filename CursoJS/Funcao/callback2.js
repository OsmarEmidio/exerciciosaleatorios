const notas = [2.0, 7.5, 6.9, 9, 8.0, 5, 7]

//sem callback
let notasbaixas = []
for(let i in notas){
    if (notas[i] < 7){
        notasbaixas.push(notas[i])
    }
}

console.log(notasbaixas)

//com callback
notasbaixas = notas.filter((nota) => nota < 7)
console.log(notasbaixas)