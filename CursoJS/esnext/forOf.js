for(let letra of "Osmar"){
    console.log(letra)
}

const assuntosEcma = ['Map', 'Set', 'Promise']
console.log('')

for (let indice in assuntosEcma){ //in imprime indice
    console.log(indice)
}
console.log('')

for (let valor of assuntosEcma){ //of imprime o valor
    console.log(valor)
}
console.log('')

const assuntosMap = new Map([
    ['Map', {abordado: true}],
    ['Set', {abordade: true}],
    ['Promise', {abordado: false}]
])

for (let assunto of assuntosMap){
    console.log(assunto)
}

for(let chave of assuntosMap.keys()){
    console.log(chave)
}

for(let valor of assuntosMap.values()){
    console.log(valor)
}

for (let [ch, vl] of assuntosMap.entries()){
    console.log(ch,vl)
}
console.log('')

const s = new Set(['a', 'b', 'c'])

for(let letra of s){
    console.log(letra)
}