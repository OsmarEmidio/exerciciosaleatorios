const pilotos = ['Vettel', 'Alonso', 'Raikkonen', 'Massa']
pilotos.pop() //remove o ultimo elemento do array

pilotos.push('Verstappen') //adiciona um elemento no final do array
console.log(pilotos)

pilotos.shift() //remove o primeiro elemento do array
console.log(pilotos)

pilotos.unshift('Hamilton') //adicona um elemento no inicio do array
console.log(pilotos)

//splice pode adicionar e remover elementos
//array.splice(indice, nº de itens a ser deletado a partir do indice indicado, elementos a serem adicionados a partir do indice indicado)

//adicionar
pilotos.splice(2, 0, 'Bottas', 'Massa')
console.log(pilotos)

//remover
pilotos.splice(2, 2)
console.log(pilotos)

const algunsPilotos = pilotos.slice(0, 3) //gera um novo array a partir do indice dado
console.log(algunsPilotos)