function soma() {
    let calculo = 0
    for(i in arguments){
        calculo += arguments[i]
    }
    return calculo
}

console.log(soma(1, 2, 3))
console.log(soma())