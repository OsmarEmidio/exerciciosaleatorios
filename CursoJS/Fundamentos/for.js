let contador = 1
while(contador <= 10){
    //console.log(`contador = ${contador}`)
    contador++
}

for(let i = 1; i <= 10; i++){
    //console.log(`contador for = ${i}`)
}

const notas = [6.7, 7.8, 8.9, 10, 9]

for(let i = 0; i < notas.length; i++){
    if(i == 3){
        continue  //o continue interrompe essa repetição e pula pra próxima, diferente do break que interrompe todas as repetições
    }
    console.log(`Nota = ${notas[i]}`)
}
console.log("------------------------------------")

for(let i in notas){
    console.log(notas[i])
}

const pessoa = {
    nome: 'Ana',
    idade: 29,
    peso: 64
}

for(let atributo in pessoa){
    console.log(`${atributo} = ${pessoa[atributo]}`)
}