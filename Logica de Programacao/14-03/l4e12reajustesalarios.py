'''
6. As Organizações Tabajara resolveram dar um aumento de salário aos seus colaboradores e lhe contraram para desenvolver o programa que calculará os reajustes.
Faça um programa que recebe o salário de um colaborador e o reajuste segundo o seguinte critério, baseado no salário atual:
-salários até R$ 280,00 (incluindo) : aumento de 20%
-salários entre R$ 280,00 e R$ 700,00 : aumento de 15%
-salários entre R$ 700,00 e R$ 1500,00 : aumento de 10%
-salários de R$ 1500,00 em diante : aumento de 5% Após o aumento ser realizado, informe na tela:
 *o salário antes do reajuste;
 *o percentual de aumento aplicado;
 *o valor do aumento;
 *o novo salário, após o aumento.
'''
s=float(input("Salario Atual: R$"))
if(s<=280):
    a1=s*0.2
    r1=s+a1
    print("Salario Antes do Reajuste: R${:.2f}".format(s))
    print("20% de Aumento")
    print("Valor do Aumento: R${:.2f}".format(a1))
    print("Novo Salario: R${:.2f}".format(r1))
elif(s<=700):
    a2=s*0.15
    r2=s+a2
    print("Salario Antes do Reajuste: R${:.2f}".format(s))
    print("15% de Aumento")
    print("Valor do Aumento: R${:.2f}".format(a2))
    print("Novo Salario: R${:.2f}".format(r2))
elif(s<=1500):
    a3=s*0.1
    r3=s+a3
    print("Salario Antes do Reajuste: R${:.2f}".format(s))
    print("10% de Aumento")
    print("Valor do Aumento: R${:.2f}".format(a3))
    print("Novo Salario: R${:.2f}".format(r3))
else:
    a4=s*0.05
    r4=s+a4
    print("Salario Antes do Reajuste: R${:.2f}".format(s))
    print("5% de Aumento")
    print("Valor do Aumento: R${:.2f}".format(a4))
    print("Novo Salario: R${:.2f}".format(r4))
