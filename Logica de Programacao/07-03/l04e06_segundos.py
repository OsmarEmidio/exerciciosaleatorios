
'''
Data= 07/03/2018

Operandos Matemáticos

Abaixo segue uma lista com os operadores aritméticos utilizados em Python:

    + → soma
    – → subtração
    * → multiplicação
    / → divisão
    // → divisão de inteiros
    ** → potenciação
    % → módulo (resto da divisão)


6 - Escreva um algoritmo que dado um período de tempo
expresso em segundos, determine o valor desse período
expresso em dias, horas, minutos e segundos.
Por exemplo, 91820 segundos corresponde a 1 dia, 1 hora, 30
minutos e 20 segundos.

Sugestão: use as operações Div ( // ) e Resto ( % ).'''




# efetua o camando para o usuário digitar os segundos
s=int(input('Quantidade de Segundos: '))

# efetua o cálculo de transformação.
h=s//60//60
d=h//60//60//24
rs=s%3600
m=rs//60
rsf=s%60
if (h>=24):
    d=int(h/24)
    h=int(h%24)
# mostra o resultado utilizando o .format da seguinte forma:
print('{} segundos corresponde a {} dia, {} hora, {} minutos e {} segundos'.format(s,d,h,m,rsf))
# 91820 segundos corresponde a 1 dia, 1 hora, 30 minutos e 20 segundos.



