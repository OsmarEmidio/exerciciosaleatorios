class Lancamento{
    constructor(nome = 'Genérico', valor = 0){
        this.nome = nome
        this.valor = valor
    }
}

class CicloFinanceiro{
    constructor(mes, ano){
        this.mes = mes
        this.ano = ano
        this.lancamentos = []
    }

    addLancamentos(lancamentos){
        lancamentos.forEach(element => this.lancamentos.push(element))
    }

    sumario(){
        let valorConsolidado = 0
        this.lancamentos.forEach(l => {
            valorConsolidado += l.valor
        })
        return valorConsolidado
    }
}

const salario = new Lancamento('Salário', 45000)
const contadeluz = new Lancamento('contaLuz', 300)
const contas = new CicloFinanceiro(6, 2018)
contas.addLancamentos(salario, contadeluz)
console.log(contas.sumario())
