// foreach passa como parametros: 1º elemento, 2º indice e 3º o próprio array, 

const aprovados = ['Agatha', 'Aldo', 'Daniel', 'Raquel']

aprovados.forEach(function(nome, indice){
    console.log(`${indice + 1}) ${nome}`)
})

console.log("")
aprovados.forEach(nome => console.log(nome))