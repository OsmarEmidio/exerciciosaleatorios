/*03) Crie uma função que recebe dois parâmetros, base e expoente, e retorne a base elevada ao expoente.*/

function elevar(base, expoente){
    let resultado = 1
    for(let i = expoente; i != 0; i--){
        resultado *= base
    }
    return resultado
}

console.log(elevar(2, 2))
console.log(elevar(2, 0))
console.log(elevar(0, 2))