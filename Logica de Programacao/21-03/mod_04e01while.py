'''
-- WHILE --

O Laço de Repetição while repete um bloco de instrução enquanto
a condição definida em seu cabeçalho for verdadeiro.

A estrutura while talvez seja a mais simples para entendermos nesse momento,
porém, não raramente encontramos alunos que dizem não entender o funcionamento
dessa estrutura. Se você entendeu o funcionamento da estrutura if, pense
na estrutura while como sendo a estrutra if mas que ao invés de executar o seu
bloco de instrução uma única vez, executará enquanto a expressão definida
for igual a True.

tradução while == enquanto

while (condição):
    executa bloco
    
'''

print("Numeros Pares de 1 a 10")
conta=0
while(conta < 10):
    conta += 2
    print(conta)

print("Numeros Impares de 0 a 10")
contai=1
while(contai<10):
      print(contai)
      contai+=2

print("Ordem Descrecente de 0 a 10")
cd=10
while(cd>=0):
    print(cd)
    cd-=1

print("Numeros Pares de 0 a 10")
conta0=0
while(conta0<=10):
    print(conta0)
    conta0+=2

print("Numeros Impares de 0 a 20")
c20=1
c20r=c20%2
while(c20<=20):
    print(c20)
    if(c20r!=0):
        c20+=2

print("Soma dos Valores Pares de 0 a 20")
cont=0
soma=0
while(cont<=20):
    soma=soma+cont
    cont+=2
    print(soma)
    
'''
a- imprime apenas os numeros pares de 0 a 10, sem utilizar a estrurara if
b- imprima apenas os numeros imprares
c- imprima da ordem inversa, começando em 10 e terminando em 0.
d- imprimir os numeros pares, começando pelo valor zero(0)
e- imprimir os valores impares de 0 a 20, utilizando a condição if dentro
do while - dica: utilizar o operador aritmético resto da divisão
f- imprima a soma dos numeros pares de 0 a 20, ultilizando a estrutura while



'''
