
'''
Data= 07/03/2018

Operandos Matemáticos

Abaixo segue uma lista com os operadores aritméticos utilizados em Python:

    + → soma
    – → subtração
    * → multiplicação
    / → divisão
    // → divisão de inteiros
    ** → potenciação
    % → módulo (resto da divisão)


4. Escreva um algoritmo para calcular o volume de uma
esfera de raio R. 
Onde:	Volume = (4 . pi . r3 ) /3  ,onde     (r3 = raio ao cubo)
'''

# bliblioteca de funcões matemáticas
import math                             # Importando os métodos de math

# recebe o raio que o usuário digitar
r=float(input('Raio da Esfera (em m): '))

# efetue o calculo do volume
vol = (4*math.pi*pow(r,3))/3

# mostra o resultado utilizando o .format
print('O Volume da Esfera é {:.2f} metro cubicos'.format(vol))


'''
a - refazer o calculo utilizando o metodo pow(x,y)
b-  refazer o calculo utilizando o valor de PI da biblioteca "math"
'''
