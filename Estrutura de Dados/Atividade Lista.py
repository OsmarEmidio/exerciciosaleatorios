import sys
class Lista():
    def __init__(self):
        global l
        self.l=[]
        l = self.l
        m=int(input("Tamanho da Lista Principal: "))
        for i in range(0,m):
            n=input("{}º Elemento da Lista: ".format(i))
            self.l.append(n)
            

class Menu():
    def __init__(self):
        print("")
        print("Opções:")
        print("0 - Sair do Programa")
        print("1 - Adicionar Elementos à Lista Principal") 
        print("2 - Remover Elementos da Lista Principal") #Questão 8
        print("3 - Alterar Elementos da Lista Principal")
        print("4 - Printar Lista Principal")
        print("5 - Criar uma Sub-Lista a Partir da Lista Principal") #Questão 1 e Questão 17
        print("6 - Endereço de um Elemento na Lista Principal") #Questão 4
        print("7 - Intercalar Duas Listas") #Questão 2
        print("8 - Concatenar Duas Listas") #Questão 3
        print("9 - Tamanho da Lista Principal") #Questão 5
        print("10 - Excluir Elementos em Branco na Lista Principal") #Questão 6
        print("11 - Verificar se uma Lista ou String é Palindromo") #Questão 9
        print("12 - Verificar na Lista Principal Quantos Elementos Há de um Determinado Numero e Seus Respectivos Endereços") #Questão 10
        print("13 - Criar Lista que é Cópia da Lista Principal com Elementos Repetidos Eliminados") #Questão 11
        print("14 - Ordenar Lista Principal")
        print("15 - Inserir Elementos em Lista Ordenada e Mantê-la Ordenada") #Questão 7
        print("16 - Intercalando Listas Ordenadas. Obs: A Lista que Resultará dessa Intercalação também será Ordenada") #Questão 12
        print("17 - Criar Lista Mostrando Quantas Vezes Cada Elemento da Lista Principal Apareceu. Obs: Aparecerá o Elemento Seguido do Nº de Vezes em que Apareceu") #Questão 13
        print("18 - Eliminar Todas as Ocorrencias de um Elemento da Lista Principal. A Lista Principal Será Ordenada no Processo") #Questão 14
        print("19 - Elemento que Aparece o Maior e o Menor nº de Vezes na Lista Principal") #Questão 15
        print("20 - Calcule o Comprimento de uma Cadeia de Caracteres de Tamanho 'n'") #Questão 16
        print("21 - Copiar a Lista Principal em outra Lista") #Questão 18
        print("22 - Remover 3 nós de uma lista de tamanho 10 a partir da posição k") #Questão 19
        print("23 - Inserir n Elementos na Posição k da Lista Principal") #Questão 20
        print("24 - Eliminar Elementos Repetidos da Lista Principal") #Questão 21
        print("")
        Menu.Opções()
        
    def Opções():
        o=int(input("Opção Escolhida: "))
        if(o == 0):
            Operações.Sair()
        if(o == 1):
            Operações.Inserir()
        if(o == 2):
            Operações.Remover()
        if(o == 3):
            Operações.Alterar()
        if(o == 4):
            Operações.Printar()
        if(o == 5):
            Operações.Sublista()
        if(o == 6):
            Operações.Endereço()
        if(o == 7):
            Operações.Intercalar()
        if(o == 8):
            Operações.Concatenar()
        if(o == 9):
            Operações.Tamanho()
        if(o == 10):
            Operações.ExcluirBranco()
        if(o == 11):
            Operações.Palindromo()
        if(o == 12):
            Operações.NumElem()
        if(o == 13):
            Operações.List_sem_Repetidos()
        if(o == 14):
              Operações.Ordenar()
        if(o == 15):
            Operações.AcrescentarOrdenada()
        if(o == 16):
            Operações.IntercalarOrdenada()
        if(o == 17):
            Operações.ElemCount()
        if(o == 18):
            Operações.Remover2()
        if(o == 19):
            Operações.Maior_Menor()
        if(o == 20):
            Operações.ComprimentoCC()
        if(o == 21):
            Operações.Copiar()
        if(o == 22):
            Operações.Remover3()
        if(o == 23):
            Operações.InserirN()
        if(o == 24):
            Operações.EliminarRepetidos()
        Menu()

class Operações():
    def Sair():
        sys.exit()
        
    def Inserir():
        print("")
        p=int(input("Quantidade de Elemontos a Serem Inseridos: "))
        for i in range(1,p+1):
            n=input("{}º Elemento a Ser Adicionado: ".format(i))
            l.append(n)
        
    def Remover():
        print("")
        r=int(input("Posição do Elemento a ser Removido: "))
        l.remove(l[r])
        
    def Alterar():
        print("")
        e=input("Novo Elemento: ")
        p=int(input("Posição do Elemento a ser Alterado"))
        l[p]=e
        
    def Printar():
        print("")
        print("Lista: ", l)
        
    def Sublista():
        print("")
        global sl
        sl=[]
        n=int(input("Tamanho da Nova Sub-Lista: "))
        p=int(input("Inicia a Partir de Qual Elemento da Lista: "))
        for j in range(0,n):
            sl.append(l[p])
            p=p+1
        print("Sub-Lista: ", sl)
        
    def Endereço():
        print("")
        n=input("Elemento: ")
        m=l.index(n)
        print("O Elemento ",n," está na posição ",m)
        
    def Intercalar():
        print("")
        global l2
        l2 = []
        m2=int(input("Tamanho da Nova Lista: "))
        for j in range(0,m2):
            n=input("{}º Elemento da Lista: ".format(j))
            l2.append(n)
        global li
        li=[]
        if(len(l)>=len(l2)):
            lim=len(l)
        else:
            lim=len(l2)
        for i in range(0,lim):
            if(i<len(l)):
                li.append(l[i])
            if(i<len(l2)):
                li.append(l2[i])
        print("Listas Intercaladas: ",li)
        
    def Concatenar():
        print("")
        global l3
        l3=[]
        m=int(input("Tamanho da Nova Lista: "))
        for j in range(0,m):
            n=input("{}º Elemento da Lista: ".format(j))
            l3.append(n)
        global lc
        lc=l+l3
        print("Listas Concatenadas: ",lc)

    def Tamanho():
        print("")
        print("Tamanho da Lista Principal: ",len(l))

    def ExcluirBranco():
        l[:] = [elemento for elemento in l if elemento != ""]
        print(l)

    def Palindromo():
        print("")
        x=input("Você deseja verificar uma string ou uma lista?  ")
        if(x == "string"):
            s=input("Digite a String a Ser Verificada: ")
            l2=list(s)
            l2r=list(l2)
            l2r.reverse()
            print("")
            if(l2 == l2r):
                print("A String {} é Palindromo".format(s))
            else:
                print("A String {} não é um Palindroms".format(s))
        if(x == "lista"):
            l1=[]
            m1=int(input("Tamanho da Lista: "))
            for i in range(0,m1):
                n1=input("{} Elemento da Lista: ".format(i))
                l1.append(n1)
            lr=(list(l1))
            lr.reverse()
            print("")
            if(l1 == lr):
                print("A Lista {} é Palindromo".format(l1))
            else:
                print(" A Lista {} não é Palindromo".format(l1))

    def NumElem():
        print("")
        e=input("Elemento: ")
        print("")
        n=l.count(e)
        le=[]
        for i in range(0,len(l)):
            if(l[i] == e):
                le.append(i)
        print("Há {} Elementos {} na Lista Principal".format(n,e))
        print("Seu(s) Respectivos Endereços são: ",le)

    def List_sem_Repetidos():
        print("")
        global lsr
        lsr=list(l)
        for i in lsr:
            if(lsr.count(i) > 1):
                lsr[:] = [elemento for elemento in lsr if elemento != i]
        print("Lista Sem Elementos Repetidos: ", lsr)

    def Ordenar():
        print("")
        l.sort()
        print("Lista Principal Ordenada: ",l)

    def AcrescentarOrdenada():
        print("")
        n=int(input("Quantidade de Elementos a Serem Inseridos: "))
        for i in range(1,n+1):
            e=input("{}º Elemento a ser Adicionado: ".format(i))
            l.append(e)
        l.sort()
        print("")
        print("Lista Principal: ",l)

    def IntercalarOrdenada():
        print("")
        lo1 = []
        m=int(input("Tamanho da 1ª Lista: "))
        for j in range(0,m):
            n=input("{}º Elemento da Lista: ".format(j))
            lo1.append(n)
        print("")
        lo2 = []
        m2=int(input("Tamanho da 2ª Lista: "))
        for k in range(0,m2):
            n=input("{}º Elemento da Lista: ".format(k))
            lo2.append(n)
        print("")
        print("Ordenando Listas...")
        lo1.sort()
        lo2.sort()
        print("")
        print("Lista 1: ",lo1)
        print("Lista 2: ",lo2)
        loi=[]
        if(len(lo1)>=len(lo2)):
            lim=len(lo1)
        else:
            lim=len(lo2)
        for i in range(0,lim):
            if(i<len(lo1)):
                loi.append(lo1[i])
            if(i<len(lo2)):
                loi.append(lo2[i])
        loi.sort()
        print("Listas Intercaladas: ",loi)

    def ElemCount():
        print("")
        lne=[]
        for i in range(0,len(l)):
            a=lne.count("Elemento: {}".format(l[i]))
            if(a < 1):
                lne.append("Elemento: {}".format(l[i]))
                c=l.count(l[i])
                lne.append("{} Vezes".format(c))
        print(lne)

    def Remover2():
        print("")
        m=input("Elemento a Ser Removido: ")
        x=len(l)
        c=0
        while(c < x):
            if(m == l[c]):
                l.remove(l[c])
                x=len(l)
            else:
                c+=1
        l.sort()
        print("Lista Principal: ",l)

    def Maior_Menor():
        print("")
        ma=[0]
        me=[1000]
        for i in range(0,len(l)):
            if(l.count(l[i]) > ma[0]):
                ma.clear()
                ma.append(l.count(l[i]))
                ma.append(l[i])
            elif(l.count(l[i]) == ma[0]):
                if(ma.count(l[i]) < 1):
                    ma.append(l[i])
        if(len(ma) == 2):
            print("O nº {} é o que aparece o maior numero de vezes. Ele aparece {} vez(es)".format(ma[1],ma[0]))
        else:
            a=ma[0]
            ma.remove(ma[0])
            print("Os numeros {} são os que aparecem o maior nº de vezes. Eles aparecem {} vez(es)".format(ma,a))
        for j in range(0,len(l)):
            if(l.count(l[j]) < me[0]):
                me.clear()
                me.append(l.count(l[j]))
                me.append(l[j])
            elif(l.count(l[j]) == me[0]):
                if(me.count(l[j]) < 1):
                    me.append(l[j])
        if(len(me) == 2):
            print("O nº {} é o que aparece o menor nº de vezes. Ele aparece {} vez(es)".format(me[1],me[0]))
        else:
            b=me[0]
            me.remove(me[0])
            print("Os numeros {} são os que aparecem o menor nº de vezes. Eles aparecem {} vez(es)".format(me,b))

    def ComprimentoCC():
        print("")
        s=input("String a ser verificada: ")
        ts=len(s)
        print("O Comprimento da String é: ",ts)

    def Copiar():
        lp2=(list(l))
        print("Lista Copiada: ",lp2)

    def Remover3():
        l10=[]
        for i in range(0,10):
           n=input("{} Elemento da Lista: ".format(i))
           l10.append(n)
        print("Lista com 10 Elementos: ",l10)
        r=int(input("Posição do Elemento a ser Excluido: "))
        for j in range(0,3):
            l10.remove(l10[r])
        print("Lista: ",l10)

    def InserirN():
        n=int(input("Numero de Elementos a serem adicionados: "))
        k=int(input("Posição a ser adicionado: "))
        for i in range(0,n):
            e=input("Elemento a ser adicionado: ")
            l.insert(k,e)
        print(l)
        
    def EliminarRepetidos():
        for i in l:
            if(l.count(i) > 1):
                l[:] = [elemento for elemento in l if elemento != i]
        print("Lista Principal Sem Elementos Repetidos: ",l)

        
Lista()
Menu()
