
'''
Data= 07/03/2018

Operandos Matemáticos

Abaixo segue uma lista com os operadores aritméticos utilizados em Python:

    + → soma
    – → subtração
    * → multiplicação
    / → divisão
    // → divisão de inteiros
    ** → potenciação
    % → módulo (resto da divisão)


4. Dados os pontos A, de coordenadas A(x1, y1),
e B, de coordenadas B(x2, y2), escreve um algoritmo que
determine a distância entre os dois pontos.

A distância entre dois pontos representados no sistema de
coordenadas cartesianas é dada por:

Onde :   dist = Raiz( (X2 - X1)² + (Y2  Y1)²  )

'''

# bliblioteca de funcões matemáticas
import math                             # Importando os métodos de math

# recebe os valores que o usuário digitar
ax=float(input('Coordenada X do ponto A: '))
ay=float(input('Coordenada Y do ponto A: '))
bx=float(input('Coordenada X do ponto B: '))
by=float(input('Coordenada Y do ponto B: '))

# efetua o cálculo da distancia entre os dois pontos
dist=math.sqrt(pow(bx-ax,2)+pow(by-ay,2))

# mostra o resultado, utilizando .fomat
print('A distancia é {:.2f}'.format(dist))

#math.sqrt() -> Raiz Quadrada

