const saudacao = 'Opa' //contexto lexico 1

function exec(){
    const saudacao = 'Fala' //contexto lexico 2
    return saudacao
}

//Objetos são grupos aninhados de pares Nome/Valor
const cliente = {
    nome: 'Pedro',
    idade: 32,
    peso: 90,
    endereco: {
        logradouro: 'Rua Muito Legal',
        numero:123
    },
    numero: 3
}

console.log(saudacao)
console.log(exec())
console.log(cliente)