function aleatorio (min, max){
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

let a = -1

do {
    a = aleatorio(-1, 10)
    console.log(`a = ${a}`)
} while(a != -1)

console.log("Fim.")