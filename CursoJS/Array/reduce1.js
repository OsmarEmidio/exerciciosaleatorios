const alunos = [
    {nome: 'João', nota: 6.5, bolsista: false},
    {nome: 'Maria', nota: 9.5, bolsista: true},
    {nome: 'Pedro', nota: 8.5, bolsista: true},
    {nome: 'Ana', nota: 5.5, bolsista: false}
]

console.log(alunos.map(a => a.nota))
const resultado = alunos.map(a => a.nota).reduce(function(acumulador, atual){
    console.log(acumulador, atual)
    return acumulador + atual
}, 0)

console.log(resultado)