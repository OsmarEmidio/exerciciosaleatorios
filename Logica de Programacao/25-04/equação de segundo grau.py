from math import sqrt

def delta(a,b,c):
    delta=(b**2)-(4*a*c)
    print("Delta é igual a ",delta)
    raizes(a,b,delta)
def raizes(a,b,delta):
    if(delta==0):
        x1=-b/2*a
        print("A Raiz é igual à ",x1)
    elif(delta<0):
        print("Delta menor que zero, sem raizes reais")
    elif(a==0):
        print("Raizes iguais a 0")
    else:
        x1=(-b+sqrt(delta))/2*a
        x2=(-b-sqrt(delta))/2*a
        print("As Raizes são iguais a ",x1," e ",x2)
a=float(input("Valor de a: "))
b=float(input("Valor de b: "))
c=float(input("Valor de c: "))
delta(a,b,c)
