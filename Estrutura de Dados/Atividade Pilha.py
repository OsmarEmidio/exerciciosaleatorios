
import sys

print("-----------------------------------------------------------------------------")
print("UNICEUB - Ciência da Computação")
print("Estrutura de Dados - 2º Semestre")
print("Aluno: Osmar Emídio")
print("-----------------------------------------------------------------------------")

class Pilha():
    def __init__(self):
        self.pilha=[]
        self.pilha2=[]
        self.pilha3=[]

    def Acrescentar1(self):
        q=int(input("Quantos elementos você deseja adicionar à 1ª pilha? "))
        for i in range(1,q+1):
            e=input("{}º Elemento a ser adicionado: ".format(i))
            self.pilha.append(e)

    def Acrescentar2(self):
        q=int(input("Quantos elementos você deseja adicionar à 2ª pilha? "))
        for i in range(1,q+1):
            e=input("{}º Elemento a ser adicionado: ".format(i))
            self.pilha2.append(e)
    
    def Remover(self):
        self.t=self.pilha.pop()

        

class Menu():
    def __init__(self):
        print("---------- PARA VER UM EXERCÍCIO DIGITE SEU RESPECTIVO NÚMERO ---------------")
        print("")
        print("Digite [0] para sair do programa")
        print("Exercício 1 - Inverter sequência de caracteres usando uma pilha")
        print("Exercício 2 - Concatenar duas pilhas gerando uma terceira")
        print("Exercício 3 - Acessar o 1º elemento de uma pilha")
        print("Exercício 4 - Converter um nº decimal em um nº binario")
        print("Exercício 5 - Converter um nº decimal em um nº hexadecimal")
        print("Exercício 6 - Inverter uma lista")
        print("Exercício 7 - Converter uma expressão infixa em pré-fixa")
        print("Exercício 8 - Converter uma expressão infixa em pós-fixa")
        print("Exercício 9 - Realizar uma analise sintatica de uma expressão aritmetica")
        print("Exercício 10 - Algoritmo que resolve Torre de Hanoi de 3 hastes e 3 discos")
        print("Exercício 11 - Algoritimo 2L de agua para uma experiência usando uma garrafa de 5L e outra de 4L")
        print("Exercício 12 - Verificar se expressões aritmeticas estão com a parentização correta") 
        print("Exercício 13 - Algoritimo para converter a expressão 'A+(B*C)' para posfixo")
        print("Exercício 14 - Verificar se palavra é palindrome")
        print("Exercício 15 - Multiplas pilhas alocadas num mesmo array")
        print("")
        global o
        o=int(input("Qual exercício você deseja ver? "))
        Menu.Opções()
        
    def Opções():
        print("")
        if(o == 0):
            sys.exit()
        if(o == 1):
            print("-----------------------------------------------------------------------------")
            o1=Ex1()
            o1.Inverte()
        if(o == 2):
            print("-----------------------------------------------------------------------------")
            o2=Ex2()
            o2.Concatenar()
        if(o == 3):
            print("-----------------------------------------------------------------------------")
            o3=Ex3()
            o3.Primeiro_Elemento()
        if(o == 4):
            print("-----------------------------------------------------------------------------")
            o4=Ex4()
            o4.Decimal_Binario()
        if(o == 5):
            print("-----------------------------------------------------------------------------")
            o5=Ex5()
            o5.Decimal_Hexadecimal()
        if(o == 6):
            print("-----------------------------------------------------------------------------")
            o6=Ex6()
            o6.Inverter_Lista()
        if(o == 7):
            print("-----------------------------------------------------------------------------")
            o7=Ex7()
            o7.Infixa_Prefixa()
        if(o == 8):
            print("-----------------------------------------------------------------------------")
            o8=Ex8()
            o8.Infixa_Posfixa()
        if(o == 9):
            print("-----------------------------------------------------------------------------")
            o9=Ex9()
            o9.Analisa_Expressao()
        if(o == 10):
            print("-----------------------------------------------------------------------------")
            Ex10()
            Ex10.Torre_Hanoi(3,'Haste A','Haste B','Haste C')
        if(o == 11):
            print("-----------------------------------------------------------------------------")
            o11=Ex11()
            o11.Garrafas()
        if(o == 12):
            print("-----------------------------------------------------------------------------")
            o12=Ex12()
            o12.Parentizacao()
        if(o == 13):
            print("-----------------------------------------------------------------------------")
            o13=Ex13()
            o13.Posfixo2()
        if(o == 14):
            print("-----------------------------------------------------------------------------")
            o14=Ex14()
            o14.Palindrome()
        if(o == 15):
            print("-----------------------------------------------------------------------------")
            o15=Ex15()
            o15.Array()
        print("")
        Menu()

class Ex1(Pilha):
    def Inverte(self):
        c=input("String a ser invertida: ")
        self.pilha2=list(c)
        while(len(self.pilha2)>0):
            t=self.pilha2.pop()
            self.pilha.append(t)
        print("Resposta: ",self.pilha)

class Ex2(Pilha):
    def Concatenar(self):
        self.Acrescentar1()
        print("")
        self.Acrescentar2()
        print("")
        print("Pilha 1: {} | Pilha 2: {}".format(self.pilha,self.pilha2))
        self.pilha3=self.pilha+self.pilha2
        print("3ª Pilha: ",self.pilha3)

class Ex3(Pilha):
    def Primeiro_Elemento(self):
        self.Acrescentar1()
        print("Pilha Criada: ",self.pilha)
        print("O 1º Elemento dessa pilha é: ", self.pilha[0])

class Ex4(Pilha):
    def Decimal_Binario(self):
        d=int(input("Numero Decimal: "))
        h=d
        while(d >= 2):
            a=d%2
            d=d/2
            d=int(d)
            self.pilha2.append(a)
        self.pilha2.append(d)
        while(len(self.pilha2)>0):
            e=self.pilha2.pop()
            self.pilha.append(e)
        juntar = ''.join(str(e) for e in self.pilha)
        print("Numero {} em Binário: {}".format(h,juntar))

class Ex5(Pilha):
    def Decimal_Hexadecimal(self):
        d=int(input("Número Decimal: "))
        h=d
        while(d >= 16):
            a=d%16
            if(a == 10):
                self.pilha2.append('A')
            elif(a == 11):
                self.pilha2.append('B')
            elif(a == 12):
                self.pilha2.append('C')
            elif(a == 13):
                self.pilha2.append('D')
            elif(a == 14):
                self.pilha2.append('E')
            elif(a == 15):
                self.pilha2.append('F')
            else:
                self.pilha2.append(a)
            d=d/16
            d=int(d)
        if(d == 10):
            self.pilha2.append('A')
        elif(d == 11):
            self.pilha2.append('B')
        elif(d == 12):
            self.pilha2.append('C')
        elif(d == 13):
            self.pilha2.append('D')
        elif(d == 14):
            self.pilha2.append('E')
        elif(d == 15):
            self.pilha2.append('F')
        else:
            self.pilha2.append(d)
        while(len(self.pilha2)>0):
            e=self.pilha2.pop()
            self.pilha.append(e)
        juntar = ''.join(str(e) for e in self.pilha)
        print("O Numero {} em Hexadecimal é: {}".format(h,juntar))

class Ex6(Pilha):
    def Inverter_Lista(self):
        t=int(input("Tamanho da Lista: "))
        for i in range(1,t+1):
            e=input("{}º Elemento da Lista: ".format(i))
            self.pilha2.append(e)
        print("")
        print("Lista: ",self.pilha2)
        while(len(self.pilha2) > 0):
            f=self.pilha2.pop()
            self.pilha.append(f)
        print("Lista Invertida: ",self.pilha)

class Ex7(Pilha):
    def Infixa_Prefixa(self):
        e=input("Expressão Infixa: ")
        self.pilha2=list(e)
        pilha4=[]
        i=0
        while(i < len(self.pilha2)):
            if(self.pilha2[i] == ')'):
                a=pilha4.pop()
                self.pilha3.append(a)
            if(self.pilha2[i] != '+' and self.pilha2[i] != '-' and self.pilha2[i] != '*' and self.pilha2[i] != '/'):
                a=self.pilha2[i]
                self.pilha.append(a)
                self.pilha2.remove(self.pilha2[i])
            elif(self.pilha2[i] == '+' or self.pilha2[i] == '-' or self.pilha2[i] == '*' or self.pilha2[i] == '/'):
                a=self.pilha2[i]
                pilha4.append(a)
                self.pilha2.remove(self.pilha2[i])
        for j in range(0,len(pilha4)):
            c=pilha4.pop()
            self.pilha3.append(c)
        self.pilha.reverse()
        for h in range(0,len(self.pilha)):
            f=self.pilha.pop()
            self.pilha3.append(f)
        print("Expressão Préfixa: ",self.pilha3)

class Ex8(Pilha):
    def Infixa_Posfixa(self):
        e=input("Expressão Infixa: ") 
        self.pilha2=list(e)
        h=0
        while(h < len(self.pilha2)):
            if(self.pilha2[h] == ')'):
                a=self.pilha3.pop()
                self.pilha.append(a)
            if(self.pilha2[h] == '*' or self.pilha2[h] == '/' or self.pilha2[h] == '+' or self.pilha2[h] == '-'):
                a=self.pilha2[h]
                self.pilha3.append(a)
                self.pilha2.remove(self.pilha2[h])
            elif(self.pilha2[h] != '*' and self.pilha2[h] != '/' and self.pilha2[h] != '+' and self.pilha2[h] != '-'):
                a=self.pilha2[h]
                self.pilha.append(a)
                self.pilha2.remove(self.pilha2[h])
        for i in range(0,len(self.pilha3)):
            b=self.pilha3.pop()
            self.pilha.append(b)
        print("Expressão Pósfixa: ",self.pilha)

class Ex9(Pilha):
    def Analisa_Expressao(self):
        e=input("Expressão Aritmetica: ")
        self.pilha2=list(e)
        for i in range(0,len(self.pilha2)):
            if(self.pilha2[i] == '('):
                self.pilha.append('(')
            elif(self.pilha2[i] == ')'):
                if(len(self.pilha) == 0):
                    print("Você fechou um parenteses que você não abriu! Sua expressão está incorreta!")
                    Menu()
                else:
                    a=self.pilha.pop()
                    if(a != '('):
                        print("Você fechou um parenteses que você não abriu! Sua expressão está incorreta!")
                        Menu()
            elif(self.pilha2[i] == '['):
                self.pilha.append('[')
            elif(self.pilha2[i] == ']'):
                if(len(self.pilha) == 0):
                    print("Você fechou um colchete que você não abriu! Sua expressão está incorreta!")
                    Menu()
                else:
                    a=self.pilha.pop()
                    if(a != '['):
                        print("Você fechou um colchete que você não abriu! Sua expressão está incorreta!")
                        Menu()
            elif(self.pilha2[i] == '{'):
                self.pilha.append('{')
            elif(self.pilha2[i] == '}'):
                if(len(self.pilha) == 0):
                    print("Você fechou uma chave que você não abriu! Sua expressão está incorreta!")
                    Menu()
                else:
                    a=self.pilha.pop()
                    if(a != '{'):
                        print("Você fechou uma chave que você não abriu! Sua expressão está incorreta!")
                        Menu()
        if(len(self.pilha) != 0):
            print("Sua Expressão está incorreta!")
        else:
            print("Sua expressão esta correta!")
            
class Ex10():
    def Torre_Hanoi(n,A,B,C):
        if(n > 0):
            Ex10.Torre_Hanoi(n-1, A, C, B)
            print("Mova o disco " + str(n) + " da " + A + " para " + C)
            Ex10.Torre_Hanoi(n-1, B, A, C)

class Ex11(Pilha):
    def Garrafas(self):
        print("Capacidade Garrafa 1: 5L| Capacidade Garrafa 2: 4L")
        print("")
        print("Enchendo Garrafa 1 na torneira...")
        self.pilha=['1L','1L','1L','1L','1L']
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Enchendo Garrafa 2 usando a Garrafa 1...")
        for i in range(0,4):
            a=self.pilha.pop()
            self.pilha2.append(a)
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Esvaziando Garrafa 2...")
        for i in range(0,len(self.pilha2)):
            self.pilha2.pop()
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Passando conteudo da Garrafa 1 para Garrafa 2...")
        a=self.pilha.pop()
        self.pilha2.append(a)
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Enchendo Garrafa 1 na torneira...")
        self.pilha=['1L','1L','1L','1L','1L']
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Enchendo Garrafa 2 usando a Garrafa 1...")
        for i in range(0,3):
            a=self.pilha.pop()
            self.pilha2.append(a)
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Esvaziando a Garrafa 2...")
        for i in range(0,len(self.pilha2)):
            self.pilha2.pop()
        print("Garrafa 1: {} | Garrafa 2: {}".format(self.pilha,self.pilha2))
        print("")
        print("Pronto, a Garrafa 1 tem exatamente 2L!!!")

class Ex12(Pilha):
    def Parentizacao(self):
        e=input("Expressão Aritmetica: ")
        self.pilha2=list(e)
        for i in range(0,len(self.pilha2)):
            if(self.pilha2[i] == '('):
                self.pilha.append('(')
            elif(self.pilha2[i] == ')'):
                if(len(self.pilha) == 0):
                    print("Você está fechando um parenteses que você não abriu! Sua parentização está incorreta!!!")
                    Menu()
                else:
                    self.pilha.pop()
        if(len(self.pilha) == 0):
            print("A parentização dessa expressão está correta!!!")
        else:
            print("A parentização dessa expressão está incorreta!!!")

class Ex13(Pilha):
    def Posfixo2(self):
        print("Expressão: A+(B*C)")
        self.pilha2=['A','+','(','B','*','C',')']
        h=0
        while(h < len(self.pilha2)):
            if(self.pilha2[h] == ')'):
                a=self.pilha3.pop()
                self.pilha.append(a)
            if(self.pilha2[h] == '*' or self.pilha2[h] == '/' or self.pilha2[h] == '+' or self.pilha2[h] == '-'):
                a=self.pilha2[h]
                self.pilha3.append(a)
                self.pilha2.remove(self.pilha2[h])
            elif(self.pilha2[h] != '*' and self.pilha2[h] != '/' and self.pilha2[h] != '+' and self.pilha2[h] != '-'):
                a=self.pilha2[h]
                self.pilha.append(a)
                self.pilha2.remove(self.pilha2[h])
        for i in range(0,len(self.pilha3)):
            b=self.pilha3.pop()
            self.pilha.append(b)
        print("Expressão 'A+(B*C)' em Pósfixa: ",self.pilha)

class Ex14(Pilha):
    def Palindrome(self):
        p=input("Palavra a ser verificada: ")
        print("")
        self.pilha2=list(p)
        for i in range(0,len(self.pilha2)):
            a=self.pilha2.pop()
            self.pilha.append(a)
        self.pilha2=list(p)
        print(self.pilha2)
        print(self.pilha)
        print("")
        if(self.pilha2 == self.pilha):
            print("Essa palavra é palindrome!!!")
        else:
            print("Essa palavra não é palindrome!!!")

class Ex15():
    def Pilhas(self):
        self.pilha=[]

    def Add_Pilha(self):
        q=int(input("Quantos elementos você deseja adicionar à {}ª pilha? ".format(self.x)))
        for i in range(1,q+1):
            e=input("{}º Elemento a ser adicionado: ".format(i))
            self.pilha.append(e)
        
    def Array(self):
        self.array=[]
        z=int(input("Numero de pilhas a serem adicionadas ao array: "))
        for self.x in range(1,z+1):
            self.Pilhas()
            self.Add_Pilha()
            self.array.append(self.pilha)
            print("")
        print("")
        print("Array Criado: ",self.array)
        print("")
        self.Modificar_Array()
        
    def Add(self):
        e=input("Elemento a ser adicionado: ")
        self.array[self.p].append(e)

    def Rem(self):
        self.t=self.array[self.p].pop()

    def AR(self):
        i=input("Você deseja adicionar[a] ou remover[r] elementos? ")
        if(i == 'a'):
            self.Add()
        elif(i == 'r'):
            self.Rem()
        else:
            print("Digito incorreto, use a letra [r] para remover e a [a] para adicionar")
            self.AR()
        print("")
        print("Array: ",self.array)
        print("")
        
    def Modificar_Array(self):
        print("Você deseja modificar qual Pilha?")
        self.p=int(input("Digite o índice da pilha ou [-1] se vc deseja parar de modificar: "))
        print()
        if(self.p==-1):
            print("Array: ",self.array)
            print("")
            Menu()
        for h in range(0,self.x):
            if(self.p == h):
                self.AR()
            else:
                pass
        self.Modificar_Array()
                

Menu()
