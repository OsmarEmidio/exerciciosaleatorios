//Operador ... (rest/spread)
//usar rest com parametro de função

//usar spread com objeto
const funcionario = {nome: 'Maria', salario: 1345}
const clone = {ativo: true, ...funcionario}
console.log(clone)

//spead com array
const grupoA = ['João', 'Pedro', 'Osmar']
const grupoFinal = ['Maria', ...grupoA, 'Rafaela']
console.log(grupoFinal)

//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa