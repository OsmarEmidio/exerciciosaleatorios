'''
data = 28/02/2018

2.  Faça um programa que leia a largura e a altura de uma parede em metros,
 calcule a sua área e a quantidade de tinta necessária para pintá-la, 
sabendo que cada litro de tinta pinta uma área de 2 metros quadrados.

		
'''


#calcular área= altura*largura.
#entrada de dados

altura=float(input('Digite a altura da parede: '))
largura=float(input('Digite a largura da parede: '))

area=altura*largura

quant_tinta= area/2

print('Para uma parede de {:.2f}m² irá precisar de {:.2f}L de tinta'.format(area,quant_tinta))

'''
Alterações:
a. Na tela de saída de dados, mostre também o valor largura e da altura, utilizando o metodo .format.

'''
