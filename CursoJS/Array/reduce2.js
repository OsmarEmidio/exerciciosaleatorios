const alunos = [
    {nome: 'João', nota: 6.5, bolsista: true},
    {nome: 'Maria', nota: 9.5, bolsista: false},
    {nome: 'Pedro', nota: 8.5, bolsista: false},
    {nome: 'Ana', nota: 5.5, bolsista: false}
]

//Desafio 1: Todos os alunos são bolsistas?

const desafio1 = alunos.map(a => a.bolsista).reduce(function(acumulador, proximo){
    if(acumulador && proximo){
        acumulador = true
    }
    else{
        acumulador = false
    }
    return acumulador
})

console.log(desafio1)

//Desafio 2: Algum aluno é bolsista?

const desafio2 = alunos.map(a => a.bolsista).reduce(function(acumulador, proximo){
    let verificador = false
    if(acumulador || proximo){
        verificador = true
    }
    return verificador
})

console.log(desafio2)