function vetor_impares_pares(vetor){
    let pares = 0
    let impares = 0
    for (let i in vetor){
        if(vetor[i]%2 == 0){
            pares++
        }
        else{
            impares++
        }
    }
    console.log(`${pares} são pares e ${impares} são impares`)
}

vetor_impares_pares([1,2,3,4,5,6,7,8,9])