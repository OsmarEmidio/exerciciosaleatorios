/*17) Um funcionário irá receber um aumento de acordo com o seu plano de
trabalho, de acordo com a tabela abaixo:
Plano | Aumento
A     | 10%
B     | 15%
C     | 20%
Faça uma função que leia o plano de trabalho e o salário atual de um funcionário e calcula e imprime o seu
novo salário. Use a estrutura switch e faça um caso default que indique que o plano é inválido*/

function aumento(salario, plano){
    switch(plano){
        case 'a':
            salario = salario + (salario * 0.1)
            return salario
        case 'b':
            salario = salario + (salario * 0.15)
            return salario
        case 'c':
            salario = salario + (salario * 0.2)
            return salario
    }
}

console.log(aumento(1000, 'a'))
console.log(aumento(1000, 'b'))
console.log(aumento(1000, 'c'))
