console.log(this === global)//false
console.loh(this === module)//false

console.log(this === module.exports)//true
console.log(this === exports)//true

function logThis(){
    console.log('Dentro de uma função...')
    console.log(this === exports)//falso, pois dentro de uma função o this assume outro valor
    console.log(this === global)//true, dentro de uma função o this assume o escopo global
} 

logThis()

